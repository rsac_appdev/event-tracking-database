# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120426183544) do

  create_table "agencies", :force => true do |t|
    t.string   "st_unit_id"
    t.string   "unit_name"
    t.string   "agency_id"
    t.string   "dispatch_id"
    t.integer  "fregion"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "fires", :force => true do |t|
    t.string   "event_id"
    t.string   "incident_id"
    t.string   "incident_name"
    t.integer  "part_of_complex"
    t.string   "incident_type"
    t.integer  "active_incident"
    t.date     "ig_date"
    t.string   "ig_lat",                    :limit => 10
    t.string   "ig_long",                   :limit => 10
    t.integer  "area_burned",                              :default => 0
    t.integer  "percent_contained"
    t.date     "expected_containment_date"
    t.date     "actual_containment_date"
    t.string   "ig_state"
    t.string   "ig_admin"
    t.string   "ig_agency"
    t.string   "ig_usfs_region"
    t.integer  "fregion"
    t.string   "gacc"
    t.string   "city"
    t.string   "state"
    t.date     "next_pass"
    t.date     "report_date"
    t.text     "comment"
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.text     "fuels"
    t.text     "general_location"
    t.integer  "complex"
    t.integer  "which_complex"
    t.integer  "path1"
    t.integer  "row1"
    t.string   "baer_status",               :limit => 45
    t.string   "mtbs_status",               :limit => 45
    t.string   "ravg_status",               :limit => 45
    t.string   "srcdb",                     :limit => 45
    t.integer  "mapping_in_progress"
    t.string   "project",                   :limit => 45
    t.integer  "ravg_publishable"
    t.integer  "baer_publishable"
    t.integer  "coords",                    :limit => nil
  end

  add_index "fires", ["event_id"], :name => "event_id"
  add_index "fires", ["incident_name"], :name => "firename"

  create_table "imports", :force => true do |t|
    t.string   "event_id"
    t.string   "incident_id"
    t.string   "incident_name"
    t.boolean  "part_of_complex"
    t.string   "incident_type"
    t.boolean  "active_incident"
    t.date     "ig_date"
    t.string   "ig_lat",                    :limit => 10
    t.string   "ig_long",                   :limit => 10
    t.integer  "area_burned"
    t.integer  "percent_contained"
    t.date     "expected_containment_date"
    t.date     "actual_containment_date"
    t.string   "ig_state"
    t.string   "ig_admin"
    t.string   "ig_agency"
    t.string   "ig_usfs_region"
    t.integer  "fregion"
    t.string   "gacc"
    t.string   "city"
    t.string   "state"
    t.date     "next_pass"
    t.date     "report_date"
    t.text     "comment"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.text     "fuels"
    t.text     "general_location"
    t.boolean  "complex"
    t.string   "which_complex"
    t.integer  "path1"
    t.integer  "row1"
    t.string   "srcdb",                     :limit => 45
    t.string   "added_to_fod",              :limit => 45
  end

  create_table "mappings", :force => true do |t|
    t.integer  "fire_id"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.string   "save_mapping"
    t.integer  "prefire_path"
    t.integer  "prefire_row"
    t.string   "prefire_scene_id"
    t.string   "postfire_sensor"
    t.integer  "postfire_path"
    t.integer  "postfire_row"
    t.string   "postfire_scene_id"
    t.string   "single_scene"
    t.string   "dnbr_offset",            :limit => 25
    t.integer  "threshold1"
    t.integer  "threshold2"
    t.integer  "threshold3"
    t.string   "validated_severity"
    t.integer  "ravg_pre_nbr"
    t.integer  "ravg_dnbr"
    t.integer  "ravg_dnbr_offset"
    t.string   "program"
    t.string   "strategy"
    t.string   "unmappable_reason"
    t.string   "perimeter_scene_id"
    t.text     "other_reason"
    t.string   "perimeter_sensor"
    t.string   "perimeter_confidence",   :limit => 25
    t.text     "perimeter_comments"
    t.string   "analysis",               :limit => 45
    t.date     "prefire_date"
    t.date     "postfire_date"
    t.date     "perimeter_date"
    t.date     "pub_date"
    t.integer  "utm_zone"
    t.float    "final_pixel_size"
    t.integer  "revised"
    t.string   "status",                 :limit => 45
    t.string   "username",               :limit => 45
    t.integer  "no_data_thresh"
    t.integer  "greenness_thresh"
    t.text     "mapping_comments"
    t.integer  "burn_bndy_ac"
    t.string   "supplementary_sensor",   :limit => 45
    t.string   "supplementary_scene_id", :limit => 45
    t.date     "supplementary_date"
    t.string   "veg_type",               :limit => 45
    t.integer  "baer_publishable"
    t.integer  "ravg_publishable"
  end

  create_table "prefire_images", :force => true do |t|
    t.integer  "mapping_id"                         :null => false
    t.string   "path"                          		:null => false
    t.datetime "image_date",                        :null => false
    t.string   "scene_id" 			                :null => false
    t.string   "sensor"                  			:null => false
  end
  
  add_index "prefire_images", ["mapping_id"], :name => "mappingid"

  create_table "postfire_images", :force => true do |t|
    t.integer  "mapping_id"                         :null => false
    t.string   "path"                          		:null => false
    t.datetime "image_date",                        :null => false
    t.string   "scene_id" 			                :null => false
    t.string   "sensor"                  			:null => false
  end
  
  add_index "postfire_images", ["mapping_id"], :name => "mappingid"

  create_table "perimeter_images", :force => true do |t|
    t.integer  "mapping_id"                         :null => false
    t.string   "path"                          		:null => false
    t.datetime "image_date",                        :null => false
    t.string   "scene_id" 			                :null => false
    t.string   "sensor"                  			:null => false
  end
  
  add_index "perimeter_images", ["mapping_id"], :name => "mappingid"

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "hashed_password"
    t.string   "salt"
    t.string   "reset_code"
    t.boolean  "isadmin",         :default => false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

end
