# README #

JRuby version 1.7.19, Torquebox 3.1.2

Development environment set up on 166.2.127.163 under D:\Websites\ETD


Notes from David Hayes:

	Rails runs under JBOSS, and Apache forwards a port to it for any request pointed to http://166.2.127.163/ETD
	
	So my workflow for ETD development is that I make changes to the app in a directory called:
	
	T:\Dev\ETD\event-tracking-database
	
	That�s hard-coded into the JBOSS setup. That�s a standard Rails app with all the Gems, etc:
	
	Then I start up JBOSS by running C:\torquebox-3.1.2\jboss\bin\standalone.bat
	
	Deployment configs located at C:\torquebox-3.1.2\jboss\standalone\deployments
	
	In order to compile the js and css, run >rake assets::precompile and copy the resulting files to public/images, public/javascripts, public/stylesheets.


live site at https://fsapps.nwcg.gov/mtbs/ETD/

staging site at http://166.2.127.163/ETD/

live site hosted at 10.1.15.37

mysql: root/@rsac...

endpoints:
 - https://fsapps.nwcg.gov/mtbs/ETD/mappings/10007877.json
 - https://fsapps.nwcg.gov/mtbs/ETD/save_burn_bndy (post endpoint)
 - https://fsapps.nwcg.gov/mtbs/ETD/mappings/list/38304.json
 
