# Enable backgroundable methods for ActiveRecord classes. Provides:
# class AModel < ActiveRecord::Base
#   always_background :a_method
# end
# 
# a_model_instance.background.another_method
if defined?(TorqueBox::Messaging::Backgroundable) && defined?(ActiveRecord::Base)
  ActiveRecord::Base.send(:include, TorqueBox::Messaging::Backgroundable)
end
#fix the stupid ActiveRecord JDBC bullshit.
# module ActiveRecord::ConnectionAdapters
#   class JdbcAdapter < AbstractAdapter
#     def explain(query, *binds)
#       ActiveRecord::Base.connection.execute("EXPLAIN #{query}", 'EXPLAIN', binds)
#     end
#   end
# end