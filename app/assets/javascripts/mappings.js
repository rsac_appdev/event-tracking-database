$(document).ready(function() {
  $(".delete").click(function(e) {
    e.preventDefault();
    url = $(this).attr('href');

    $.get(url, {}, function(data) {
      var updateModal = $('#update-modal');
      updateModal.html(data);
      updateModal.modal('toggle');
    });
  });
});

