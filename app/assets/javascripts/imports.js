$(function() {
  // limit this to checkboxes on the IRWIN Imports page.
  $('checkbox.import-picker').change(function() {
    this.checked = true;
    $(':checkbox').not(this).attr('checked',false);
  });

  $('.disabled').click(function(e) {
    e.preventDefault();
  });

  $('.update-fod').click(function(e) {
    e.preventDefault();
    url = $(this).attr('href');
    $.get(url, {}, function(data) {
      var updateModal = $('#update-modal');
      updateModal.html(data);
      updateModal.modal('toggle');
      $('#cancel').on('click', function() {
        updateModal.modal('hide');
      });
      $('#update-submit').on('click',function() {
        $('#pick-incident').submit();
      });
    });
  });

  $('.delete-incident').click(function(e) {
    e.preventDefault();
    url = $(this).attr('href');
    $.get(url, {}, function(data) {
      var updateModal = $('#update-modal');
      updateModal.html(data);
      updateModal.modal('toggle');
      $('#cancel').on('click', function() {
        updateModal.modal('hide');
      });
      $('#delete').on('click',function() {

      });
    });
  });
});

