// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .
//= require bootstrap-datepicker


function activateNavbar() {
  var path = location.pathname.replace (/^(\/.+?\/.+?)\/.+$/, '$1');
  $('ul.nav li').removeClass("active");
  $('ul.nav a[href="' + path + '"]').parent().addClass("active");
}

// make sure we've actually selected a complex if we've checked the
// 'part of complex' checkbox.
function validateForm() {
  if($('#fire_part_of_complex').is(':checked') && !$('#fire_which_complex').val()){
    alert('You must also select a complex.');
    return false;
  }
  else{
    return true;
  }  
}

$(function(){
  activateNavbar();

  // The "Is This A Complex?" and "Part Of Other Complex?" boxes can't both
  // be checked at once, and the "Which Fire" dropdown needs to be disabled
  // if the fire is part of a complex.
  $('#fire_complex').click(function(e) {
    if(($(this).is(':checked')) && ($("#fire_part_of_complex").is(':checked'))){
      alert('\'Is This A Complex?\' and \'Part Of Other Complex?\' cannot both be checked!');
      $(this).prop('checked', false);
    } else {
      if($(this).is(':checked')){
        $('#fire_which_complex').val('');
        $('#fire_which_complex').attr('disabled', 'disabled');
      }
    }
  });

  $('#fire_part_of_complex').click(function(e) {
    if(($("#fire_complex").is(':checked')) && ($(this).is(':checked'))){
      alert('\'Is This A Complex?\' and \'Part Of Other Complex?\' cannot both be checked!');
      $(this).prop('checked', false);
    } else {
      if($(this).is(':checked')){
        $('#fire_which_complex').removeAttr('disabled');
      }
      else {
        $('#fire_which_complex').val('');
        $('#fire_which_complex').attr('disabled', 'disabled');
      }
    }
  });

});

$(function() {
  $('#fires').tablesorter({
      headers:{
        4:{sorter:false},
        5:{sorter:false},
        6:{sorter:false},
        7:{sorter:false},
        8:{sorter:false},
        9:{sorter:false},
        10:{sorter:false}
      }
  });
  // $(".collapse").collapse('hide');
  // $(".collapse").on('shown', function() {

  // });
  $('#imports').tablesorter({
   headers:{
    5:{sorter:false}
   }
  });
});

$(document).ready(function() {

  if(!$('#fire_part_of_complex').is(':checked')) {
    $('#fire_which_complex').attr('disabled', 'disabled');
  }

  $("#optional-fire-fields input").focus(function() {
    $(".required").each(function() {
      if(!$(this).val()) {
        $('#createEventId').click(function(e) {
          e.preventDefault();
        });
      } else {
        $("#required-fire-fields").removeClass("alert-error");
        $("#required-fire-fields").addClass("alert-success");
      }
    });
  });


  $('.filter').click(function(e) {
    e.preventDefault();
    var action = $(this).attr('href');
    $('#fires tr.fire').each(function() {
      if (!$(this).hasClass(action)) {
        $(this).hide();
      } else {
        $(this).show();
      }
    });
  });

  // $("#fire_ig_state").change(function() {
  //   var currentAcreage = $("#fire_area_burned").val();
  //   var western_states = ['AK', 'WA', 'OR', 'CA', 'MT', 'ID', 'NV', 'AZ', 'UT', 'WY', 'CO', 'NM', 'ND', 'SD', 'NE', 'KS', 'OK', 'TX'];
  //   if ($.inArray($(this).val(), western_states) > 0) {
  //     $("#fire_area_burned").val(900);
  //   } else {
  //     $("#fire_area_burned").val(450);
  //   };
  //   $("#acres_modified_warning").text(" WARNING: Acreage was changed from " + currentAcreage);
  // });


});


