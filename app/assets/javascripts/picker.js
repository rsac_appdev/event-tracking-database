$(function() {

  // this only gets enabled if someone has selected a fire.
  $(document).ready(function() {
    $('#update-submit').attr('disabled', 'disabled');
  });
                    
  $('input[name="fire_id"]').change(function(e) {
    e.stopPropagation();
    if($(this).is(':checked')){
      $('#update-submit').removeAttr('disabled');
    } else {
      $('#update-submit').attr('disabled', 'disabled');
    }
  });

  $('#picker_complex').click(function(e) {
    e.stopPropagation();
    if(($(this).is(':checked')) && ($("#picker_part_of_complex").is(':checked'))){
      alert('\'Is This A Complex?\' and \'Part Of Other Complex?\' cannot both be checked!');
      $(this).prop('checked', false);
    } else {
      if($(this).is(':checked')){
        $('#picker_which_complex').val('');
        $('#picker_which_complex').attr('disabled', 'disabled');
      }
    }
  });

  $('#picker_part_of_complex').click(function(e) {
    e.stopPropagation();
    if(($("#picker_complex").is(':checked')) && ($(this).is(':checked'))){
      alert('\'Is This A Complex?\' and \'Part Of Other Complex?\' cannot both be checked!');
      $(this).prop('checked', false);
    } else {
      if($(this).is(':checked')){
        $('#picker_which_complex').removeAttr('disabled');
      } else {
        $('#picker_which_complex').val('');
        $('#picker_which_complex').attr('disabled', 'disabled');
      }
    }
  });

  // form validator
  $('#pick-incident').submit (function(e) {
    if(!$('input[name="fire_id"]')) {
      e.preventDefault();
      alert('You must pick a fire.');
    } else {
      if($('#picker_part_of_complex').is(':checked') && !$('#picker_which_complex').val()){
        e.preventDefault();
        alert('You must also select a complex.');
      } else{
        return;
      }
    }
  });

});

