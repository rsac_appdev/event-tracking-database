class Mapping < ActiveRecord::Base
  belongs_to :fires
  has_many :prefire_images
  has_many :postfire_images
  has_many :perimeter_images
  # attr_accessible :burn_bndy_ac
  def folder_name
    "#{program}_#{id}"
  end

end
