class Datafile < ActiveRecord::Base
  def self.save(filename,upload)
    name =  filename
    directory = "public/services"
    # create the file path
    path = File.join(directory, name)
    # write the file
    File.open(path, "wb") { |f| f.write(upload.tempfile.read) }
  end
end