class Pathrow < ActiveRecord::Base
end

class Fire < ActiveRecord::Base

  has_many :mappings
  has_many :projects

  # this is the activerecord equivalent of an SQL join to the fires table (AKA
  # a self join). see:
  # http://danielchangnyc.github.io/blog/2013/11/06/self-referential-associations/
  # and
  # http://stackoverflow.com/questions/19803759/how-do-i-create-a-self-referential-association-self-join-in-a-single-class-usi
  # for an explanation.
  has_many :consituents, class_name: "Fire"
  belongs_to :parent_complex, class_name: "Fire", foreign_key: "which_complex"

  validates :ig_state, :presence => true
  validates :ig_lat, :presence => true
  validates :ig_long, :presence => true
  validates :ig_date, :presence => true

  attr_accessor :added_to_fod

  def self.get_path_rows(lat,lng)
    lng = lng.to_f
    if lng > 0
      lng = lng - (lng * 2)
    end

    ## fc6 version
    #pathrow_combos = Pathrow.find_by_sql("SELECT path, row FROM primary_pathrows WHERE WITHIN(GeomFromText('POINT(#{lng.to_s} #{lat})'), SHAPE)")

    ## this is needed in MySQL on the new EROS server.
    pathrow_combos = Pathrow.find_by_sql("SELECT path, row FROM primary_pathrows WHERE ST_Contains(SHAPE, ST_GeomFromText('POINT(#{lng.to_s} #{lat})', 1))")
    pathrows = Hash.new()
    pathrows[:path1] = pathrow_combos[0].path
    pathrows[:row1] = pathrow_combos[0].row

    return pathrows
  end

  def self.check_threshold(state, acres)
    states = %w[AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI]

    western_states = %w[AK WA OR CA HI MT ID NV AZ UT WY CO NM ND SD NE KS OK TX]
    #EASTERN_STATES = STATES - WESTERN_STATES
    if western_states.include?(state)
      logger.debug "Western state detected: #{state}."
      if acres.to_f >= 1000
        logger.debug "Acreage above threshold #{acres.to_s}."
        return 1
      end
    else
      logger.debug "Eastern state detected: #{state}"
      if acres.to_f >= 500
        logger.debug "Acreage above threshold #{acres.to_s}."
        return 1
      end
    end
    logger.debug "Acreage of #{acres.to_s} does not meet the criteria for #{state}."
    return 0;
  end

  def self.fix_lng(lng)
    lng = lng.to_f
    if lng > 0
      lng = lng - (lng * 2)
    end
    return lng
  end

  # def self.generate_event_id(state,lat,lng,ig_date)
  #   lng = lng.to_f #have to make SURE this is a float
  #   # ig_date = ig_date.to_s #cause Rails wants to magically convert this
  #   ymd = ig_date.strftime("%Y%m%d")
  #   lat_as_float = lat.to_f
  #   lat_array = lat_as_float.to_s.split('.')
  #   lat_1 = lat_array[0].rjust(2,'0')
  #   lat_2 = lat_array[1][0..2].ljust(3,'0')
  #   long_as_float = lng.to_f.abs
  #   long_array = long_as_float.to_s.split('.')
  #   long_1 = long_array[0].rjust(3,'0')
  #   long_2 = long_array[1][0..2].ljust(3,'0')
  #   event_id = "#{state}#{lat_1}#{lat_2}#{long_1}#{long_2}#{ymd}"

  #   return event_id
  # end

  def self.generate_event_id(state,lat,lng,ig_date)
    #build the RSAC ID
    # time = Date.strptime ig_date, '%m/%d/%y'
    # ymd = time.strftime("%Y%m%d")
    ymd = ig_date.strftime("%Y%m%d")
    lng = lng.gsub('-', '')
    fixed_lat = ("%2.3f" % lat).to_s.gsub('.', '')
    fixed_lng = ("%07.3f" % lng).to_s.gsub('.', '')
    event_id = "#{state}#{fixed_lat}#{fixed_lng}#{ymd}"
    return event_id
  end

  def self.csv_header
    "event_id,
    incident_id,
    incident_name,
    part_of_complex,
    which_complex,
    incident_type,
    active_incident,
    ig_date,
    ig_lat,
    ig_long,
    area_burned,
    percent_contained,
    expected_containment_date,
    actual_containment_date,
    ig_state,
    ig_admin,
    ig_agency,
    ig_usfs_region,
    fregion,
    gacc,
    report_date,
    comment,
    fuels,
    general_location,
    srcdb".split(',')
  end

  def self.build_from_csv(row)
    ig_long = row[9].to_f
    parsed_ig_date = Date.strptime(row[7], "%m/%d/%Y").to_time
    event_id = generate_event_id(row[14], row[8], ig_long, parsed_ig_date)
    # find existing customer from email or create new
    # fire = find_or_initialize_by_incident_name(row[2])
    fire = find_or_initialize_by_incident_name(row[2])
    fire.attributes ={:event_id => event_id,
                      :incident_id => row[1],
                      :incident_name => row[2],
                      :part_of_complex => row[3],
                      :which_complex => row[4],
                      :incident_type => row[5],
                      :active_incident => row[6],
                      :ig_date => parsed_ig_date.strftime("%Y-%m-%d"),
                      :ig_lat => row[8],
                      :ig_long => row[9],
                      :area_burned => row[10],
                      :percent_contained => row[11],
                      :expected_containment_date => row[12],
                      :actual_containment_date => row[13],
                      :ig_state => row[14],
                      :ig_admin => row[15],
                      :ig_agency => row[16],
                      :ig_usfs_region => row[17],
                      :fregion => row[18],
                      :gacc => row[19],
                      :report_date => row[20],
                      :comment => row[21],
                      :fuels => row[22],
                      :general_location => row[23],
                      :srcdb => row[24],
                      :created_at => Time.now,
                      :updated_at => Time.now
                  }
    return fire
  end

  def self.predict_analysis(latitude,longitude)
    ## fc6 version
    ##typeResult = ActiveRecord::Base.connection.execute("SELECT type FROM preliminary_assessment_shapes where ST_INTERSECTS(shape,POINT(#{ActiveRecord::Base.connection.quote(longitude)}, #{ActiveRecord::Base.connection.quote(latitude)}))")

    # this is also for the EROS version of FC6.
    typeResult = ActiveRecord::Base.connection.execute("SELECT type FROM preliminary_assessment_shapes WHERE ST_INTERSECTS(SHAPE, ST_GeomFromText('POINT(#{longitude} #{latitude})', 2))")

    ##typeResult = ActiveRecord::Base.connection.execute("SELECT type FROM preliminary_assessment_shapes where ST_INTERSECTS(shape,ST_GeomFromText(POINT(#{ActiveRecord::Base.connection.quote(longitude)},#{ActiveRecord::Base.connection.quote(latitude)})))")


    unless typeResult.empty?
        type = typeResult.first()["type"]
        return "Extended Assessment" if type == "EA"
        return "Initial Assessment" if type == "IA"
    end

    distance_in_miles = 10.0

    latitude_radians = latitude.to_f * Math::PI / 180
    longitude_radians = longitude.to_f * Math::PI / 180
    distance_in_degrees = distance_in_miles / (Math.cos(latitude_radians)*69).abs

    envelope_point1_x = longitude.to_f - distance_in_degrees
    envelope_point1_y = latitude.to_f - (distance_in_miles / 69)
    envelope_point2_x = longitude.to_f + distance_in_degrees
    envelope_point2_y = latitude.to_f + (distance_in_miles / 69)

    query = <<-END
        select
            SUBSTRING_INDEX(mappings.strategy,' ',1) as strategy_text,
            COUNT(*)
        from
            mappings,
            fires
        where
            mappings.program = 'MTBS'
                and fires.id = mappings.fire_id
                and st_within(fires.coords,
                    Buffer(
                        Point(#{ActiveRecord::Base.connection.quote(longitude)},#{ActiveRecord::Base.connection.quote(latitude)}),
                        10/abs(cos(radians(#{ActiveRecord::Base.connection.quote(latitude)}))*69)
                    )
                )
        group by
            strategy_text;
    END

    results = ActiveRecord::Base.connection.execute(query)
    logger.debug results

    results_map = results.flat_map{ |item| {item["strategy_text"].downcase=>item["COUNT(*)"]}}.reduce({},:update)

    ea_calls = results_map['extended'].to_f
    logger.debug "EA_CALLS" + ea_calls.to_s
    ia_calls = results_map['initial'].to_f
    logger.debug "IA_CALLS" + ia_calls.to_s
    total = results_map.values.sum.to_f
    logger.debug "Total" + total.to_s
    if total >=4 and ea_calls != 0 and ia_calls != 0
        return "Extended Assessment" if ea_calls / total >= 0.66667
        return "Initial Assessment" if ia_calls / total >= 0.66667
        return "Indeterminate"
    end

    return "Extended Assessment" if ea_calls >= 4 and ia_calls == 0
    return "Initial Assessment" if ia_calls >= 4 and ea_calls == 0
    return "Indeterminate"
  end
end
