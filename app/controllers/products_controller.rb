class ProductsController < ApplicationController
    def save_product
        @product = Product.new
        @product.mapping_id = params['mapping_id'];
        @product.name = params['name']
        @product.save

        render :nothing => true
    end

    def delete_products
        Product.where(:mapping_id => params['mapping_id']).destroy_all
        render :nothing => true
    end

    def delete_product
        Product.where(:mapping_id => params['mapping_id'],:name=>params['name']).destroy_all
        render :nothing => true
    end
end