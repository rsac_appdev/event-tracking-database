class QachecksController < ApplicationController

  def get_check
    @qacheck = Qacheck.find_by_mapping_id(params['mapping_id'])
    respond_to  do |format|
      format.html
      format.json do
        render :json => @qacheck
      end
    end
  end

  def save
    if params['id'] == ""
      @qacheck = Qacheck.new
    else
      @qacheck = Qacheck.find(params['id'])
    end
    @qacheck.mapping_id = params['mapping_id']
    @qacheck.assessment_strategy = params['assessment_strategy']
    @qacheck.assessment_strategy_meets_guidelines = params['assessment_strategy_meets_guidelines']
    @qacheck.mapping_strategy = params['mapping_strategy']
    @qacheck.mapping_strategy_meets_guidelines = params['mapping_strategy_meets_guidelines']
    @qacheck.ia_dnbr_acq_timing = params['ia_dnbr_acq_timing']
    @qacheck.ia_dnbr_acq_timing_meets_guidelines = params['ia_dnbr_acq_timing_meets_guidelines']
    @qacheck.ea_dnbr_acq_timing = params['ea_dnbr_acq_timing']
    @qacheck.ea_dnbr_acq_timing_meets_guidelines = params['ea_dnbr_acq_timing_meets_guidelines']
    @qacheck.ia_ss_acq_timing = params['ia_ss_acq_timing']
    @qacheck.ia_ss_acq_timing_meets_guidelines = params['ia_ss_acq_timing_meets_guidelines']
    @qacheck.ea_ss_acq_timing = params['ea_ss_acq_timing']
    @qacheck.ea_ss_acq_timing_meets_guidelines = params['ea_ss_acq_timing_meets_guidelines']
    @qacheck.ia_severity_thresholding = params['ia_severity_thresholding']
    @qacheck.ia_severity_thresholding_meets_guidelines = params['ia_severity_thresholding_meets_guidelines']
    @qacheck.ea_severity_thresholding = params['ea_severity_thresholding']
    @qacheck.ea_severity_thresholding_meets_guidelines = params['ea_severity_thresholding_meets_guidelines']
    @qacheck.severity_class_proportions = params['severity_class_proportions']
    @qacheck.severity_class_proportions_meets_guidelines = params['severity_class_proportions_meets_guidelines']
    @qacheck.checklist_complete = params['checklist_complete']
    if @qacheck.save
      render :text => 'Success.'
    else
      render :text => 'Failed to import JSON stream'
    end
  end

end
