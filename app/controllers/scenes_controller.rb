class ScenesController < ApplicationController

  skip_before_filter :verify_authenticity_token

  def save_scene
    @scene = Scene.new
    @scene.fire_id = params['fire_id']
    @scene.scene_id = params['scene_id']
    @scene.path = params['path']
    @scene.row = params['row']
    @scene.image_type = params['image_type']
    @scene.image_date = params['image_date']
    @scene.sensor = params['sensor']
    if @scene.save
      render :text => 'Success.'
    else
      render :text => 'Failed to import JSON stream'
    end
  end

  def list_scenes
    @scenes = Scene.where("fire_id = ?", params['fire_id'])
    respond_to  do |format|
      format.html
      format.json do
        render :json => @scenes
      end
    end
  end

end
