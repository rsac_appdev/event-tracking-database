
class PrefireImage < ActiveRecord::Base
	attr_accessor :prefire_images
end

class PostfireImage < ActiveRecord::Base
	attr_accessor :postfire_images
end

class PerimeterImage < ActiveRecord::Base
	attr_accessor :perimeter_images
end


class MappingsController < ApplicationController
  skip_before_filter :verify_authenticity_token

  #caches_page :index

  def index
    @mappings = Mapping.where("fire_id = ?", params[:id])
	
	@mappings = @mappings.map do |mapping|
		@prefire_images = PrefireImage.find_by_sql(["SELECT * from prefire_images WHERE mapping_id = ?", mapping.id])
		@postfire_images = PostfireImage.find_by_sql(["SELECT * from postfire_images WHERE mapping_id = ?", mapping.id])
		@perimeter_images = PerimeterImage.find_by_sql(["SELECT * from perimeter_images WHERE mapping_id = ?", mapping.id])
		
		mapping = mapping.as_json.merge(:prefire_images => @prefire_images).merge(:postfire_images => @postfire_images).merge(:perimeter_images => @perimeter_images)
	end
	
    respond_to  do |format|
      format.html
      format.json do
        render :json => @mappings
      end
    end
  end

  
  
  def all

    @mappings = Mapping.find_by_sql("SELECT mappings.*, fires.event_id FROM mappings INNER JOIN fires ON fires.id = mappings.fire_id order by mappings.fire_id desc")
	
	@mappings = @mappings.map do |mapping|
		@prefire_images = PrefireImage.find_by_sql(["SELECT * from prefire_images WHERE mapping_id = ?", mapping.id])
		@postfire_images = PostfireImage.find_by_sql(["SELECT * from postfire_images WHERE mapping_id = ?", mapping.id])
		@perimeter_images = PerimeterImage.find_by_sql(["SELECT * from perimeter_images WHERE mapping_id = ?", mapping.id])
		
		mapping = mapping.as_json.merge(:prefire_images => @prefire_images).merge(:postfire_images => @postfire_images).merge(:perimeter_images => @perimeter_images)
	end
	
    respond_to  do |format|
      format.html
      format.json do
        render :json => @mappings
      end
    end
  end

  
  def all_by_event
    @mappings = Mapping.find_by_sql("SELECT mappings.program, mappings.id, fires.event_id FROM mappings
                                    INNER JOIN fires ON fires.id = mappings.fire_id")

	@mappings = @mappings.map do |mapping|
		@prefire_images = PrefireImage.find_by_sql(["SELECT * from prefire_images WHERE mapping_id = ?", mapping.id])
		@postfire_images = PostfireImage.find_by_sql(["SELECT * from postfire_images WHERE mapping_id = ?", mapping.id])
		@perimeter_images = PerimeterImage.find_by_sql(["SELECT * from perimeter_images WHERE mapping_id = ?", mapping.id])
		
		mapping = mapping.as_json.merge(:prefire_images => @prefire_images).merge(:postfire_images => @postfire_images).merge(:perimeter_images => @perimeter_images)
	end
	
    respond_to  do |format|
      format.html
      format.json { render :json => @mappings }
    end
  end

  
  
  
  
  


  

  
  
  def show
    @mappings = Mapping.find_by_sql(["SELECT mappings.*, fires.old_mtbs_fire_id, fires.event_id, fires.incident_name, fires.ig_admin, fires.fregion, fires.path1, fires.row1, fires.incident_type, ig_usfs_region FROM mappings
                                    INNER JOIN fires ON fires.id = mappings.fire_id
                                    WHERE mappings.id = ? LIMIT 1", params[:id]])
						
	@mapping = @mappings.map do |mapping|
		@prefire_images = PrefireImage.find_by_sql(["SELECT * from prefire_images WHERE mapping_id = ?", mapping.id])
		@postfire_images = PostfireImage.find_by_sql(["SELECT * from postfire_images WHERE mapping_id = ?", mapping.id])
		@perimeter_images = PerimeterImage.find_by_sql(["SELECT * from perimeter_images WHERE mapping_id = ?", mapping.id])
		
		mapping = mapping.as_json.merge(:prefire_images => @prefire_images).merge(:postfire_images => @postfire_images).merge(:perimeter_images => @perimeter_images)
	end
		

    respond_to  do |format|
      format.html
      format.json { render :json => @mapping[0]}
    end
  end



  
  
  
  
  def new
    @mapping = Mapping.new
  end
  
  def save_mapping
    #expire_page :action => :index

    if ! params.has_key?('id')
      @mapping = Mapping.new
    elsif params['id'] == ""
      @mapping = Mapping.new
    else
      @mapping = Mapping.find(params['id'])
    end
	
    @mapping.fire_id = params['fire_id']
    @fire = Fire.find_by_id(params['fire_id'])
    @fire.mapping_in_progress = 1

    # these can no longer be null.
    @mapping.ravg_publishable = params['ravg_publishable'] || 0
    @mapping.baer_publishable = params['baer_publishable'] || 0
    @mapping.nps_publishable = params['nps_publishable'] || 0
    @mapping.misc_publishable = params['misc_publishable'] || 0

    @mapping.single_scene = params['single_scene']
    #if incident is designated as single scene, a supplementary image may be used
    #to create a DNBR. We'll save that info to different fields.
    if @mapping.single_scene == "1"
      @mapping.supplementary_sensor = params['prefire_sensor']
      @mapping.supplementary_scene_id = params['prefire_scene_id']
      @mapping.supplementary_date = params['prefire_date']
    else
      @mapping.prefire_sensor = params['prefire_sensor']
      @mapping.prefire_scene_id = params['prefire_scene_id']
      @mapping.prefire_date = params['prefire_date']
    end
    @mapping.postfire_sensor = params['postfire_sensor']
    @mapping.postfire_scene_id = params['postfire_scene_id']
    @mapping.postfire_date = params['postfire_date']
    @mapping.perimeter_sensor = params['perimeter_sensor']
    @mapping.perimeter_scene_id = params['perimeter_scene_id']
    @mapping.perimeter_sensor = params['perimeter_sensor']
    @mapping.perimeter_date = params['perimeter_date']
    @mapping.veg_type = params['veg_type']
	@mapping.prefire_row = params['prefire_row']
	@mapping.prefire_path = params['prefire_path']
	@mapping.postfire_row = params['postfire_row']
	@mapping.postfire_path = params['postfire_path']
    
    #extended mapping attributes
    @mapping.dnbr_offset = params['dnbr_offset']
    @mapping.sd_offset = params['sd_offset']
    @mapping.perimeter_confidence = params['perimeter_confidence']
    @mapping.perimeter_comments = params['perimeter_comments']
    @mapping.threshold1 = params['threshold1']
    @mapping.threshold2 = params['threshold2']
    @mapping.threshold3 = params['threshold3']
    @mapping.no_data_thresh = params['no_data_thresh']
    @mapping.greenness_thresh = params['greenness_thresh']
          
    @mapping.mapping_comments = params['mapping_comments']
    @mapping.baer_mapping_comments = params['baer_mapping_comments']
    @mapping.ravg_mapping_comments = params['ravg_mapping_comments']
    @mapping.nps_mapping_comments = params['nps_mapping_comments']
    @mapping.misc_mapping_comments = params['misc_mapping_comments']

    @mapping.status = params['status']

    if params.has_key?('revised_code')
        if not params['revised_code'].blank?
            @mapping.revised_code = params['revised_code']
            @mapping.pub_date_bkp = @mapping.pub_date unless @mapping.pub_date.nil?
            @mapping.pub_date = nil

        elsif not @mapping.revised_code.nil?
            @mapping.revised_code = 'Original'    # no null
        #    @mapping.pub_date = @mapping.pub_date_bkp
        end
    else
        @mapping.revised_code = 'Original' unless @mapping.revised_code   # no null
    end


    if params['program'] == "BAER"
      @fire.baer_status = @mapping.status
      if @fire.baer_complete.blank?
        if @mapping.status == 'complete'
          @fire.baer_complete = Time.now
        end
      end
    end
    if params['program'] == "MTBS"
      @fire.mtbs_status = @mapping.status
      if @fire.mtbs_complete.blank?
        if @mapping.status == 'complete'
          @fire.mtbs_complete = Time.now
        end
      end
    end
    if params['program'] == "RAVG"
      @fire.ravg_status = @mapping.status
      if @fire.ravg_complete.blank?
        if @mapping.status == 'complete'
          @fire.ravg_complete = Time.now
        end
      end
    end


    if params['program'] == "NPS"
      @fire.nps_status = @mapping.status
      if @fire.nps_complete.blank?
        if @mapping.status == 'complete'
          @fire.nps_complete = Time.now
        end
      end
    end
    if params['program'] == "MISC"
      @fire.misc_status = @mapping.status
      if @fire.misc_complete.blank?
        if @mapping.status == 'complete'
          @fire.misc_complete = Time.now
        end
      end
    end
    
    @mapping.validated_severity = params['validated_severity']
    # @fire.ravg_complete = params['ravg_complete']
    @mapping.program = params['program']
    @mapping.analysis = params['analysis']
    @mapping.strategy = params['strategy']

    unless params['username'].nil?
      @mapping.username = params['username']
    end
	
	resultmsg = '';

    if @mapping.save
      @fire.save!
      resultmsg = 'Success.'
    else
      resultmsg = 'Failed to import JSON stream'
    end
	
	
	if params.has_key?('id')
		PrefireImage.where(:mapping_id => params[:id]).destroy_all
		PostfireImage.where(:mapping_id => params[:id]).destroy_all
		PerimeterImage.where(:mapping_id => params[:id]).destroy_all
	end

    if not params['prefire_images'].blank?
		params['prefire_images'].each do |img|
			@prefire_img = PrefireImage.new
			@prefire_img['mapping_id'] = @mapping.id
			@prefire_img['path'] = img['path']
			@prefire_img['image_date'] = img['image_date']
			@prefire_img['scene_id'] = img['scene_id']
			@prefire_img['sensor'] = img['sensor']
			@prefire_img['location_path'] = img['location_path']
			@prefire_img['location_row'] = img['location_row']
			@prefire_img.save!
		end
    end
	
    if not params['postfire_images'].blank?
		params['postfire_images'].each do |img|
			@postfire_img = PostfireImage.new
			@postfire_img['mapping_id'] = @mapping.id
			@postfire_img['path'] = img['path']
			@postfire_img['image_date'] = img['image_date']
			@postfire_img['scene_id'] = img['scene_id']
			@postfire_img['sensor'] = img['sensor']
			@postfire_img['location_path'] = img['location_path']
			@postfire_img['location_row'] = img['location_row']
			@postfire_img.save!
		end
    end
	
    if not params['perimeter_images'].blank?
		params['perimeter_images'].each do |img|
			@perimeter_img = PerimeterImage.new
			@perimeter_img['mapping_id'] = @mapping.id
			@perimeter_img['path'] = img['path']
			@perimeter_img['image_date'] = img['image_date']
			@perimeter_img['scene_id'] = img['scene_id']
			@perimeter_img['sensor'] = img['sensor']
			@perimeter_img['location_path'] = img['location_path']
			@perimeter_img['location_row'] = img['location_row']
			@perimeter_img.save!
		end
    end
	
	render :text => resultmsg
	
  end

  def save_burn_bndy
    @mapping = Mapping.find(params['id'])
    @mapping.burn_bndy_ac = params['burn_bndy_ac']
    @mapping.utm_zone = params['utm_zone']
    @mapping.burn_bndy_lat = params['burn_bndy_lat']
    @mapping.burn_bndy_lon = params['burn_bndy_lon']
    @mapping.burn_bndy_alb_x = params['burn_bndy_alb_x']
    @mapping.burn_bndy_alb_y = params['burn_bndy_alb_y']
    @mapping.final_pixel_size = params['final_pixel_size']
    if @mapping.save!
      render :text => 'Success'
    else
      render :text => 'Fail'
    end
  end

  def get
    @mapping = Mapping.find(params['id'])
    render( :partial => 'mappings/delete_confirmation' )
  end

  def destroy
    @mapping = Mapping.find(params[:id])
    fire_id = @mapping.fire_id
    @fire = Fire.find(fire_id)
    program = @mapping.program
    if @mapping.destroy
      #firesCount = Mapping.where(fire_id: fire_id, program: program, status: "complete").count
      #status = firesCount == 0 ? "not-assessed" : "complete"
      
      firesCountComplete = Mapping.where(fire_id: fire_id, program: program, status: "complete").count
      firesCountInProgress = Mapping.where(fire_id: fire_id, program: program, status: "in-progress").count
      
      if firesCountComplete > 0 && firesCountInProgress == 0
        status = "complete"
      elsif  firesCountComplete == 0 && firesCountInProgress == 0
        status = "not-assessed"
      else 
        status = "in-progress"
      end  

      case program
      when "BAER"
        @fire.baer_status = status
        @fire.save!
      when "MTBS"
        @fire.mtbs_status = status
        @fire.save!
      when "RAVG"
        @fire.ravg_status = status
        @fire.save!
      else

      end

      Product.where(:mapping_id => params[:id]).destroy_all
      PrefireImage.where(:mapping_id => params[:id]).destroy_all
      PostfireImage.where(:mapping_id => params[:id]).destroy_all
      PerimeterImage.where(:mapping_id => params[:id]).destroy_all
	  Qacheck.where(:mapping_id => params[:id]).destroy_all

      respond_to  do |format|
        format.html {
          redirect_to(:controller => 'fires', :action => 'show', :id => fire_id)
          flash[:notice] = "Mapping has been deleted."
        }
        format.json { head :ok }
      end
    end
  end


end
