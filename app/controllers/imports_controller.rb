class ImportsController < ApplicationController

  require 'open3'

  skip_before_filter :verify_authenticity_token
  before_filter :confirm_logged_in#, :except => [:update, :add_to_fod, :update_fod, :delete, :destroy]

  def index
    # This query will only report fires above MTBS threshold
    @imports = Import.where('area_burned > ?', 450).order("incident_name ASC").order('report_date DESC').all
    # This query will report ALL incidents regardless of size
    #@imports = Import.where('id > ?', 0).order("incident_name ASC").order('report_date DESC').all
    respond_to  do |format|
      format.html do
        @pager = false
        render('index')
      end
      format.json do
        render :json => @imports
      end
    end
  end

  def today
    @imports = Import.where("created_at >= ?", Time.zone.now.beginning_of_day).order('created_at DESC')
    respond_to  do |format|
      format.html do
        render('index')
      end
      format.json do
        render :json => @imports
      end
    end
  end

  def ingest
    results = ''
    errors = ''

    # we were overflowing the 4KB space for cookies, so let's print only
    # a fraction of this output.
    Open3.popen3('f:/Websites/ETD-import/import.bat') do |stdin,stdout,stderr|
      stdout.each do |line|
        if /^Created/.match(line)
          results << line + ' '
        end
      end
      stderr.each do |line|
        next if /deprecated/.match(line)
        errors << line + ' '
      end
    end

    errors = '[none]' unless errors.length > 0

    redirect_to(:controller => 'imports', :action => 'index')
    flash[:alert] = "Manual ingestion results: #{results}. Errors: #{errors}"
  end

  def edit
    @import = Import.find(params[:id])
  end

  def update
    # expire_page :action => :index
    @import = Import.find(params[:id])
    #@import.attributes = params[:import]
    @import.attributes = import_params

    if @import.save
      redirect_to(:controller => 'imports', :action => 'index')
      flash[:alert] = "Import has been updated."
    else
      render('index')
      flash[:error] = "Import could NOT be updated"
    end
  end

  def add_to_fod
    @import = Import.find(params[:id])
    @fire = Fire.new
    @fire.attributes = @import.attributes.except('id', 'added_to_fod', 'parentComplexIrwinID', 'irwin_modifiedondatetime')

    # david says: i have no idea where this params[:complex] comes from. it may no
    # longer be used...
    if params[:complex] or @import.complex
      @fire.complex = 1
    else
      @fire.complex = 0
    end

    #logger.debug('~~~~ wtf?? ' + @import.complex.to_s + ' ' + @fire.complex.to_s)

    @fire.baer_status = "not-assessed"
    @fire.mtbs_status = "not-assessed"
    @fire.ravg_status = "not-assessed"
    @fire.nps_status = "not-assessed"
    @fire.misc_status = "not-assessed"

    @now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
    @fire.updated_at = @now
    @fire.last_gatekeeper_update = @now
    @fire.predicted_strategy = Fire.predict_analysis(@fire.ig_lat,@fire.ig_long)

    if @fire.save
      if Import.find(@import.id).destroy
        redirect_to(:controller => 'imports', :action => 'index')
        flash[:alert] = "Incident has been added to the FOD."
      else
        redirect_to(:controller => 'imports', :action => 'index')
        flash[:error] = "Could not remove incident from Gatekeeper"
      end


    else
      redirect_to(:controller => 'imports', :action => 'index')
      flash[:error] = "Incident could NOT be added"
    end
  end

  def pick_incident
    @import = Import.find(params[:id])

    @fires = Fire.where("extract(year from ig_date) = extract(year from ?) AND (event_id = ? OR incident_name LIKE ?)", \
             @import.ig_date, @import.event_id, "%#{@import.incident_name}%").order("report_date DESC")
    @complexes = Fire.where("complex = 1 AND extract(year from ig_date) = extract(year from ?)", \
                 @import.ig_date).order("incident_name ASC")

    # if the import record's incident_name contains the string COMPLEX, we assume
    # that the incident is a COMPLEX.
    @is_complex = /complex/i.match( @import.incident_name ) ? 1 : 0;
    logger.debug '++++ ' + @import.incident_name + ' ' + @is_complex.to_s + ' ++++'

    # figure out if the fire that's associated with this import is part of a complex. if
    # it is, figure out which complex it's a part of. here's how: 
    # for a given import record, find every fire that has its same event_id (there should
    # only be one, but jenn says duplicates sometimes sneak in). then, for those fires,
    # see if they are part of a complex by checking if their 'which_complex' field isn't
    # empty. if they are part of a complex (and here's where things get especially 
    # kludge-y...), find the complex they're part of via fire.parent_complex.id. then,
    # push those parent fires to @which_complex. the first value in @which_complex (and
    # remember - there should only be one) will be the first Which Complex? <option> value
    # in the "pick incident" form. note also that we remove the @which_complex values from
    # @complexes so they don't appear as options twice.
    # 
    # yup. this is a kludge.

    @is_part_of_complex = 0      # no longer used. nuke if it stays unused.
    @which_complex = Array.new
    
    # temp_fires is all of the existing fires that have the same event_id
    # as this import. there should only be one!
    temp_fires = Fire.where("event_id = ?", @import.event_id)
    
    if ! temp_fires.empty?
      temp_fires.each do |fire|
        if fire.which_complex 
          @is_part_of_complex = 1
          tmp = Fire.find(fire.parent_complex.id)
          if tmp
            @which_complex.push(tmp)
            logger.debug '++++' + @is_part_of_complex.to_s + ' ' + tmp.incident_name
          end
        end
      end
      # else - do nothing. but this means that there's no parent complex.
    end

    # remove the parent from the list of other complexes.
    # @complexes = @complexes - @which_complex

    #let's throw up a modal if the fire doesn't exist
    if @fires.empty?
      render( :partial => 'imports/update_error' )
    else
      render( :partial => 'imports/pick_incident', :locals => { :fires => @fires })
    end

  end

  def update_fod
    # will turn off once we're sure this stuff is working as intended.
    logger.debug 'XXXXX - update_fod - XXXXX'
    logger.debug 'Fire ID ' + params[:fire_id].to_s
    logger.debug 'Import ID ' + params[:import_id].to_s
    logger.debug 'Complex ' + params[:picker_complex].to_s
    logger.debug 'Part of complex '+ params[:picker_part_of_complex].to_s
    logger.debug 'Which complex ' + params[:picker_which_complex].to_s

    @fire = Fire.find(params['fire_id'])
    @import = Import.find(params['import_id'])

    status_message = "Incident saved. Event ID not updated."

    unless @fire.mapping_in_progress == 1
      logger.info "Generating new Event ID"
      @fire.event_id = @import.event_id
      status_message = "Incident updated and Event ID changed."
      @fire.baer_status = "not-assessed"
      @fire.mtbs_status = "not-assessed"
      @fire.ravg_status = "not-assessed"
      @fire.nps_status = "not-assessed"
      @fire.misc_status = "not-assessed"
    end

    @fire.incident_id = @import.incident_id
    @fire.incident_name = @import.incident_name
    @fire.irwinID = @import.irwinID
    @fire.ig_date = @import.ig_date
    @fire.ig_lat = @import.ig_lat
    @fire.ig_long = @import.ig_long
    @fire.area_burned = @import.area_burned
    @fire.percent_contained = @import.percent_contained
    @fire.expected_containment_date = @import.expected_containment_date
    @fire.actual_containment_date = @import.actual_containment_date
    @fire.ig_state = @import.ig_state
    @fire.ig_admin = @import.ig_admin
    @fire.ig_agency = @import.ig_agency
    @fire.fregion = @import.fregion
    @fire.fuels = @import.fuels
    @fire.general_location = @import.general_location
    @fire.path1 = @import.path1
    @fire.row1 = @import.row1
    @fire.srcdb = @import.srcdb
    @fire.last_gatekeeper_update = @import.report_date
    @fire.firecode = @import.firecode

    @now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
    @fire.updated_at = @now
    @fire.last_gatekeeper_update = @now

    # ???????
    @fire.complex = @import.complex

    # what's going on here:
    # quoting jlecker:
    # - if a fire has previously been complexed display the complex it belongs
    # to in the "Which Complex?" column on the right and don't allow the default
    # complex designations (listed below) to overwrite it
    # otherwise:
    # - if a fire is part of a complex, it gets its above_mapping_threshold
    # from the complex of which it's a part.
    # - if a fire is a complex, we need to find all of the constituent fires
    # that comprise it and set their AMT's accordingly.
    # - and if it's just a plain ol' fire, its above_mapping_threshold is
    # its and its alone.
    #
    # also - if a fire is not part of a complex, its which_complex must be nil.

    if @fire.part_of_complex != 1
      if params[:picker_part_of_complex]
        @fire.part_of_complex = 1
        @fire.which_complex = params[:picker_which_complex]
        @fire.complex = 0
        @which_complex = Fire.find(params[:picker_which_complex])
        @fire.above_mapping_threshold = @which_complex.above_mapping_threshold
      else
        @fire.part_of_complex = 0
        @fire.which_complex = nil
        @fire.above_mapping_threshold = Fire.check_threshold(@fire.ig_state, @fire.area_burned)

        # get the constituent fires that make up this complex and (maybe)
        # change their above_mapping_threshold values.
        if params[:picker_complex]
          @fire.complex = 1
          Fire.where("part_of_complex = 1 AND which_complex = ?", @fire.id).each do |constituent|
            logger.debug 'found constituent ' + constituent.id.to_s
            if constituent.above_mapping_threshold != @fire.above_mapping_threshold
              logger.debug '  changing!'
              constituent.above_mapping_threshold = @fire.above_mapping_threshold
              if constituent.save
                logger.debug '  saved!'
              else
                logger.debug '  errors: ' + constituent.errors
              end
            end
          end
        else
          @fire.complex = 0
        end
      end
    else
      parent_id = Fire.find(@fire.parent_complex.id)
      if parent_id
        logger.debug 'Found Parent ID for setting AMT: ' + parent_id.id.to_s
        @fire.above_mapping_threshold = parent_id.above_mapping_threshold
      else
        logger.debug 'CAN\'T FIND PARENT ID FOR SETTING AMT!'
      end
    end           # end 'if @fire.part_of_complex....'


    if @fire.save
      if Import.find(@import.id).destroy
        redirect_to(:controller => 'imports', :action => 'index')
        flash[:alert] = status_message
      else
        redirect_to(:controller => 'imports', :action => 'index')
        flash[:error] = "Could not remove incident from Gatekeeper"
      end
    else
      redirect_to(:controller => 'imports', :action => 'index')
      flash[:error] = "Incident could NOT be updated"
    end
  end

  def delete
    @import = Import.find(params[:id])
    render( :partial => 'import_del' )
  end

  def destroy
    Import.find(params['id']).destroy
    redirect_to(:controller => 'imports', :action => 'index')
    flash[:alert] = "Incident has been deleted."
  end

  private

  def import_params

    params.require(:import).permit(
    :event_id, :incident_id, :incident_name, :part_of_complex, :incident_type,
    :active_incident, :ig_date, :ig_lat, :ig_long, :area_burned, :percent_contained,
    :expected_containment_date, :actual_containment_date, :ig_state, :ig_admin,
    :ig_agency, :ig_usfs_region, :fregion, :gacc, :city, :state, :next_pass,
    :report_date, :comment, :created_at, :updated_at, :fuels, :general_location,
    :complex, :which_complex, :path1, :row1, :path2, :row2, :srcdb, :mapping_in_progress,
    :mtbs_status, :ravg_status, :baer_status, :added_to_fod, :firecode)

  end

end


