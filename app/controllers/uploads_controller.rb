class UploadsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  
  def prefire_scene
    raw_post_data = request.raw_post
    post = Datafile.save('prefire.json',params[:file])
    if params[:file]
      render :nothing => true
    else
      render :text => 'Error Uploading File.'
    end
  end
end
