require 'csv'

# caches_page :index

class FiresController < ApplicationController
  def index
    unless params['days'].nil?
      @num_days = params['days'].to_i
    else
      @num_days = 30
    end

    @fires = Fire.where("ig_date >= ?", @num_days.day.ago).order("ig_date DESC")
    sql = @fires.to_sql
    respond_to  do |format|
      format.html do
        #flash[:alert] = "Incidents within the last #{@num_days} days.<br> Query executed: #{sql}".html_safe
        flash[:alert] = ""
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end
  end

  def get_srcdbs
    @fires = Fire.find_by_sql("SELECT DISTINCT srcdb FROM fires");
    respond_to  do |format|
      format.html do
        flash[:alert] = "Source DBs"
        render('index')
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end
  end

  def search_by_year
    @fires = Fire.where("ig_date BETWEEN ? AND ?", "#{params['year']}-01-01", "#{params['year']}-12-31").order("ig_date DESC")
    sql = @fires.to_sql
    respond_to  do |format|
      format.html do
        #flash[:alert] = "Incidents from #{params['year']}.<br>Query executed: #{sql}".html_safe
        flash[:alert] = ""
        render('index')
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end
  end

  def advanced_search
    @search = Fire.search(params[:q])
    @fires = @search.result
    @search.build_condition if @search.conditions.empty?
    @search.build_sort if @search.sorts.empty?
  end

  def advanced_results
    @search = Fire.search(params[:q])
    @fires = @search.result
    sql = @fires.to_sql
    respond_to  do |format|
      format.html do
        #flash[:alert] = "Query executed: #{sql}".html_safe
        flash[:alert] = ""
        render('index')
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end
  end

  def export_all
    @fires = Fire.where("created_at > ?", '2000-01-01')
    export(@fires) #method is defined in lib/export.rb
  end

  def search
    @fires = Fire.where("incident_name LIKE ? OR event_id = ?", "%#{params['incident_name']}%", params['incident_name'])
    sql = @fires.to_sql
    respond_to  do |format|
      format.html do
        @results = true
        #flash[:alert] = "Incidents by name.<br> Query executed: #{sql}".html_safe
        flash[:alert] = ""
        render('index')
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end
  end

  def search_by_name
    @fires = Fire.where("incident_name LIKE ? OR event_id = ?", "%#{params['incident_name']}%", params['incident_name'])
    sql = @fires.to_sql
    respond_to  do |format|
      format.html do
        #flash[:alert] = "Incidents by name.<br> Query executed: #{sql}".html_safe
        flash[:alert] = ""
        @results = true
        render('index')
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end
  end
  
def search_by_mapping_id
	mappingId = params['mappingId']
	@fires = Fire.find_by_sql(["SELECT fires.* from fires INNER JOIN mappings ON fires.id = mappings.fire_id WHERE mappings.id = ?",mappingId ])
	
	respond_to  do |format|
	  format.html do
		@results = true
		#flash[:alert] = "Incidents by name.<br> Query executed: #{sql}".html_safe
		flash[:alert] = ""
		render('index')
	  end
	  format.csv do
		export(@fires) #method is defined in lib/export.rb
	  end
	  format.json do
		render :json => @fires
	  end
	end
end

  
  def date_form

  end

  def search_by_dates
    date_type = params['date_type']
    date_start = params['date_start']
    date_end = params['date_end']
    @fires = Fire.where("#{date_type} BETWEEN ? AND ?", date_start, date_end)
    respond_to  do |format|
      format.html do
        flash[:alert] = "Incidents with #{date_type} between #{date_start} and #{date_end}."
        @results = true
        render('index')
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end
  end

  def search_by_pr
    path = params['pr'][0..1]
    row = params['pr'][2..3]
    year = params['year']
    if params['ignore_threshold'] == "1"
      @fires = Fire.where("path1 = ? AND row1 = ?", path, row)
    else
      @fires = Fire.where("path1 = ? AND row1 = ? AND above_mapping_threshold = 1", path, row)
    end

    respond_to  do |format|
      format.html do
        @results = true
        render('index')
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end
  end

  def search_by_pr_year
    path = params['pr'][0..1]
    row = params['pr'][2..3]

    if params['srcdb'] == 'ALL'
      if params['ignore_threshold'] == "1"
        @fires = Fire.where("(path1 = ? AND row1 = ?) AND (ig_date BETWEEN '#{params['year']}-01-01' AND '#{params['year']}-12-31')", path, row)
      else
        @fires = Fire.where("(path1 = ? AND row1 = ?) AND above_mapping_threshold = 1 AND (ig_date BETWEEN '#{params['year']}-01-01' AND '#{params['year']}-12-31')", path, row)
      end
    else
      if params['ignore_threshold'] == "1"
        @fires = Fire.where("srcdb = #{srcdb} AND (path1 = ? AND row1 = ?) AND (ig_date BETWEEN '#{params['year']}-01-01' AND '#{params['year']}-12-31')", path, row)
      else
        @fires = Fire.where("srcdb = #{srcdb} AND (path1 = ? AND row1 = ?) AND above_mapping_threshold = 1 AND (ig_date BETWEEN '#{params['year']}-01-01' AND '#{params['year']}-12-31')", path, row)
      end

    end

    respond_to  do |format|
      format.html do
        @results = true
        render('index')
      end
      format.csv do
        export(@fires) #method is defined in lib/export.rb
      end
      format.json do
        render :json => @fires
      end
    end

  end

  def get_complexes
    @complexes = Fire.where("complex = 1").order("incident_name ASC")
    render( :partial => 'fires/complexes' )
  end

  def show
    @fire = Fire.find(params[:id])
    @mappings = Mapping.where("fire_id = ?", params[:id])
    @complexes = Fire.where("which_complex = ?", @fire.id)
    @complex = Fire.where("id = ?", @fire.which_complex)
    respond_to do |format|
      format.html
      format.json do
        render :json => @fire
      end
    end
  end

  def get_incident
    @fire = Fire.find_by_incident_id(params[:incident_id])
    respond_to do |format|
      format.html
      format.json do
        render :json => @fire
      end
    end
  end

  def new
    @fire = Fire.new
    @projects = Project.all.map{|x| [x.name, x.id]}
  end

  def create
    # expire_page :action => :index

    @fire = Fire.new(fire_params)

    @fire.baer_status = "not-assessed"
    @fire.mtbs_status = "not-assessed"
    @fire.ravg_status = "not-assessed"
    @fire.nps_status = "not-assessed"
    @fire.misc_status = "not-assessed"
    @fire.event_id = Fire.generate_event_id(@fire.ig_state,@fire.ig_lat, @fire.ig_long, @fire.ig_date)
    @fire.ig_long = Fire.fix_lng(@fire.ig_long)
    pr = Fire.get_path_rows(@fire.ig_lat, @fire.ig_long)
    @fire.path1 = pr[:path1]
    @fire.row1 = pr[:row1]
    if (not params[:fire].has_key?('incident_name')) or params[:fire]['incident_name'] == ''
      @fire.incident_name = "UNNAMED"
    end

    # if fire name includes COMPLEX, it's a complex.
    if /complex/i.match(@fire.incident_name)
      @fire.complex = 1
    else
      @fire.complex = 0
    end

    if params[:which_complex] == ''
      @fire.part_of_complex = 0
    end
    @fire.above_mapping_threshold = Fire.check_threshold(@fire.ig_state, @fire.area_burned)
    @fire.predicted_strategy = Fire.predict_analysis(@fire.ig_lat,@fire.ig_long)
    if @fire.save
      redirect_to(:controller => 'fires', :action => 'index')
      flash[:alert] = "Fire has been saved."
    else
      render('new')
      flash[:error] = "Fire could NOT be added"
    end
  end

  def edit
    @fire = Fire.find(params[:id])
    logger.debug @fire.ig_date
    @complexes = Fire.find_by_sql(["SELECT CONCAT(incident_name, ' - ', event_id) AS incident_name, id \
                 FROM fires WHERE (complex = 1) AND extract(year from ig_date) = extract(year from ?) \
                 ORDER BY incident_name ASC", @fire.ig_date]).map {|x| [x.incident_name, x.id]}
    @projects = Project.all.map{|x| [x.name, x.id]}
  end

  def update
    # expire_page :action => :index
    @fire = Fire.find(params[:id])
    @fire.attributes = fire_params
    ###params[:fire].each {|key, value| logger.debug "#{key} is #{value}" }

    # what's going on here:
    # - if a fire is part of a complex, it gets its above_mapping_threshold
    # from the complex of which it's a part.
    # - if a fire is a complex, we need to find all of the constituent fires
    # that comprise it and set their AMT's accordingly.
    # - and if it's just a plain ol' fire, its above_mapping_threshold is
    # its and its alone.
    #
    # also - if a fire is not part of a complex, its which_complex must be nil.

    if @fire.complex
      logger.debug "Complex = " + @fire.complex.to_s
    else
      logger.debug "Complex = NIL"
    end

    if @fire.which_complex
      logger.debug "Which complex = " + @fire.which_complex.to_s
    else
      logger.debug "Which complex = NIL"
    end

    if @fire.part_of_complex == 1
      @which_complex = Fire.find(@fire.which_complex)
      if @which_complex
        logger.debug 'Got it!'
        @fire.above_mapping_threshold = @which_complex.above_mapping_threshold
      else
        logger.debug 'No complex: ' + @fire.which_complex.to_s
      end
    else
      @fire.which_complex = nil
      @fire.above_mapping_threshold = Fire.check_threshold(@fire.ig_state, @fire.area_burned)

      # get the constituent fires that make up this complex and (maybe)
      # change their above_mapping_threshold values.
      if @fire.complex
        Fire.where("part_of_complex = 1 AND which_complex = ?", @fire.id).each do |constituent|
          logger.debug 'found constituent ' + constituent.id.to_s
          if constituent.above_mapping_threshold != @fire.above_mapping_threshold
            logger.debug '  changing!'
            constituent.above_mapping_threshold = @fire.above_mapping_threshold
            if constituent.save
              logger.debug '  saved!'
            else
              logger.debug '  errors: ' + constituent.errors
            end
          end
        end
      end
    end

    logger.debug '***************************************************************************************'
    logger.debug @fire.mapping_in_progress

    unless @fire.mapping_in_progress == 1
      @fire.event_id = Fire.generate_event_id(@fire.ig_state,@fire.ig_lat, @fire.ig_long, @fire.ig_date)
      logger.info "Generating new Event ID"
    end

    if @fire.save
      logger.debug "++++++ Fire has been updated. +++++++"
      redirect_to(:controller => 'fires', :action => 'index')
      flash[:alert] = "Fire has been updated."
    else
      logger.debug "++++++ Fire could NOT be updated. +++++++"
      logger.debug @fire.errors.full_messages
      render('new')
      flash[:error] = "Fire could NOT be updated"
    end

  end

  def set_status
    @fire = Fire.find(params[:id])
    case params[:program]
      when "BAER"
        @fire.baer_status = params[:status]
        @fire.baer_reason = params[:reason]
        @fire.baer_other = params[:other]
        if @fire.baer_complete.blank?
          if params[:status] == 'complete'
            @fire.baer_complete = Time.now
          end
        end

      when "MTBS"
        @fire.mtbs_status = params[:status]
        @fire.mtbs_reason = params[:reason]
        @fire.mtbs_other = params[:other]
        if @fire.mtbs_complete.blank?
          if params[:status] == 'complete'
            @fire.mtbs_complete = Time.now
          end
        end

      when "RAVG"
        @fire.ravg_status = params[:status]
        @fire.ravg_reason = params[:reason]
        @fire.ravg_other = params[:other]
        if @fire.ravg_complete.blank?
          if params[:status] == 'complete'
            @fire.ravg_complete = Time.now
          end
        end

      when "NPS"
        @fire.nps_status = params[:status]
        @fire.nps_reason = params[:reason]
        @fire.nps_other = params[:other]
        if @fire.nps_complete.blank?
          if params[:status] == 'complete'
            @fire.nps_complete = Time.now
          end
        end

      when "MISC"
        @fire.misc_status = params[:status]
        @fire.misc_reason = params[:reason]
        @fire.misc_other = params[:other]
        if @fire.misc_complete.blank?
          if params[:status] == 'complete'
            @fire.misc_complete = Time.now
          end
        end
      else

        
      end

    if @fire.save
      redirect_to(:controller => 'fires', :action => 'index')
      flash[:alert] = "Fire has been updated."
    else
      render('new')
      flash[:error] = "Fire could NOT be updated"
    end
  end

  def set_strategy

    @fire = Fire.find(params[:id])
    @fire.validated_strategy = params[:strategy]
    logger.debug "Saved " + @fire.save.to_s
    render :nothing => true
  end
  
  def set_mtbs_status
    @fire = Fire.find(params[:id])
    @fire.mtbs_status = params[:mtbs_status]
    logger.debug "Saved MTBS status " + @fire.save.to_s
    render :nothing => true
  end  

  def set_ravg_status
    @fire = Fire.find(params[:id])
    @fire.ravg_status = params[:ravg_status]
    logger.debug "Saved RAVG status " + @fire.save.to_s
    render :nothing => true
  end 

  def set_baer_status
    @fire = Fire.find(params[:id])
    @fire.baer_status = params[:baer_status]
    logger.debug "Saved BAER status " + @fire.save.to_s
    render :nothing => true
  end 

  def set_nps_status
    @fire = Fire.find(params[:id])
    @fire.nps_status = params[:nps_status]
    logger.debug "Saved NPS status " + @fire.save.to_s
    render :nothing => true
  end 

  def set_misc_status
    @fire = Fire.find(params[:id])
    @fire.misc_status = params[:misc_status]
    logger.debug "Saved MISC status " + @fire.save.to_s
    render :nothing => true
  end 

  def calculate_prediction
    @fire = Fire.find(params[:id])
    @fire.predicted_strategy = Fire.predict_analysis(@fire.ig_lat,@fire.ig_long)
    @fire.save
    render :nothing => true
  end

  def import

  end

  def upload
    if request.post? && params[:file].present?
      infile = params[:file].read
      n, errs = 0, []
      # type = params[:type]
      CSV.parse(infile) do |row|
        n += 1
        # SKIP: header i.e. first row OR blank row
        next if n == 1 or row.join.blank?
        # build_from_csv method will map fire attributes &
        # build new fire record
        fire = Fire.build_from_csv(row)
        # Save upon valid
        # otherwise collect error records to export
        if fire.valid?
          fire.save
        else
          logger.debug "row is invalid"
          errs << row
        end
      end
      # Export Error file for later upload upon correction
      if errs.any?
        errFile ="errors_#{Date.today.strftime('%d%b%y')}.csv"
        errs.insert(0, Fire.csv_header)
        errCSV = CSV.generate do |csv|
          errs.each {|row| csv << row}
        end
        flash[:error] = "CSV didn't upload"
        send_data errCSV,
          :type => 'text/csv; charset=iso-8859-1; header=present',
          :disposition => "attachment; filename=#{errFile}.csv"
      else
        flash[:notice] = "CSV Uploaded Successfully."
        redirect_to(:action => 'index')
      end
    end
  end

  def delete
    @fire = Fire.find(params['id'])
    render( :partial => 'fires/delete_confirmation' )
  end

  def destroy
    @fire = Fire.find(params[:id])
    if @fire.event_id == params['event-id-input']
      @fire.destroy
      redirect_to(:controller => 'fires', :action => 'index')
      flash[:notice] = "Fire has been deleted."
    else
      redirect_to(:controller => 'fires', :action => 'index')
      flash[:error] = "Event ID not valid for this fire. Please try again."
    end

  end

  private

  def fire_params

    params.require(:fire).permit(
    :event_id, :incident_id, :incident_name, :part_of_complex, :incident_type,
    :active_incident, :ig_date, :ig_lat, :ig_long, :area_burned, :percent_contained,
    :expected_containment_date, :actual_containment_date, :ig_state, :ig_admin,
    :ig_agency, :ig_usfs_region, :fregion, :gacc, :city, :state, :next_pass,
    :report_date, :comment, :created_at, :updated_at, :fuels, :general_location,
    :complex, :which_complex, :path1, :row1, :path2, :row2, :srcdb, :mapping_in_progress,
    :mtbs_status, :ravg_status, :baer_status, :nps_status, :misc_status,
    :above_mapping_threshold, :irwinID, :project_id, :firecode)

  end

end
