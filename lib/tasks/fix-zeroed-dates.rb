require 'rubygems'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

incidents = Fire.where("report_date BETWEEN '0012-01-01' AND '0012-12-31'")

incidents.each do |incident|
  date_to_fix = incident.report_date.to_s
  date_array = date_to_fix.split("-")
  incident.report_date = "2012-#{date_array[1]}-#{date_array[2]}"
  if incident.save
    puts "saved #{incident.incident_name} with new date of #{incident.report_date}"
  else
    puts "Couldn't save #{incident.incident_name}!"
  end
end

