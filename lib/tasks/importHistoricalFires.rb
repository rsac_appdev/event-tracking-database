require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'
require 'net/http'
require 'json'

# 1. event_id – use generate_event_id()
# 2. complex – if the word ‘complex’ is in the incident name this field gets populated
# 3. path/row – calculated spatially by lat/long use get_path_row() make sure GeoCoderNode service is running on FC6
# 4. mtbs_status, baer_status, ravg_status – set all to ‘not-assessed’ for any new fire data you import
# 5. above_mapping_threshold – 450(east) & 900(west) acres, respectively use check_threshold()
# 6. coords –  are set from a trigger in the MySQL database

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

def fix_date(date)
  time = Date.strptime date, '%m/%d/%y'
  date_string = time.strftime("%Y-%m-%d")
end

def generate_event_id(state,lat,lng,ig_date)
  #build the RSAC ID/Event ID
  time = Date.strptime ig_date, '%m/%d/%y'
  ymd = time.strftime("%Y%m%d")
  lng = lng.gsub('-', '')
  fixed_lat = ("%2.3f" % lat).to_s.gsub('.', '')
  fixed_lng = ("%07.3f" % lng).to_s.gsub('.', '')
  event_id = "#{state}#{fixed_lat}#{fixed_lng}#{ymd}"
  return event_id
end

def get_path_row(lat, lng)
  #takes lat/lng combo and returns a ruby object representing path/row/state
  #param1: (float) lat
  #param2: (float) lng
  #RETURNS:
  #(obj) path: (int)
  #      row: (int)
  #      state: (string)

  service_url = 'http://svinetfc6.fs.fed.us/geocode'
  full_url = service_url + "/" + lat.to_s + "/" + lng.to_s

  uri = URI.parse(full_url)
  response = Net::HTTP.get_response(uri)

  case response
    when Net::HTTPSuccess
      return JSON.parse(response.body)
    when Net::HTTPRedirection
      return nil
    else
      return nil
  end

end

def check_threshold(state, acres)
  states = %w[AL AK AZ AR CA CO CT DE FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY]

  western_states = %w[AK WA OR CA HI MT ID NV AZ UT WY CO NM ND SD NE KS OK TX]
  #EASTERN_STATES = STATES - WESTERN_STATES
  if western_states.include?(state)
    #it's a western state
    if acres.to_f >= 900
      return 1
    end
  else
    #its NOT a western state
    if acres.to_f >= 450
      return 1
    end
  end
  return 0;
end

CSV.open("Fires_4_import_2_ETD_complete.csv", "r").each do |slice|
  fire = Fire.new
  fire.event_id = slice[1].upcase
  fire.incident_id = slice[2]
  fire.incident_name = slice[3].upcase
  fire.part_of_complex = slice[4]
  fire.incident_type = slice[5]
  fire.active_incident = slice[6]
  fire.ig_date = slice[7]
  fire.ig_lat = slice[8]
  fire.ig_long = slice[9]
  fire.area_burned = slice[10]
  fire.ig_state = slice[14] 
  fire.ig_admin = slice[15]
  fire.ig_agency = slice[16]
  fire.gacc = slice[19]
  fire.complex = slice[26]
  pr = get_path_row(fire.ig_lat, fire.ig_long)
  fire.path1 = pr['path']
  fire.row1 = pr['row']
  fire.mtbs_status = slice[31]
  fire.srcdb = slice[33].upcase unless slice[33].nil?
  fire.mapping_in_progress = slice[34]
  fire.ravg_publishable = slice[36]
  fire.baer_publishable = slice[37]
  fire.above_mapping_threshold = slice[44]
  fire.mtbs_complete = slice[46]
  fire.old_mtbs_fire_id = slice[49]

  if ARGV[0] == '-d'
    puts "Testing fire #{fire.incident_name} - #{fire.event_id}"
  else
    if fire.save
      puts "Saved #{fire.incident_name} - #{fire.event_id}"
    else
      puts "Could not save #{fire.event_id}!"
    end
  end


end
