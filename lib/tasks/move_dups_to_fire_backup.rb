require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

dups = CSV.read('IDs_4_deletion_nameDups.csv')

class Fire < ActiveRecord::Base
end

class FireBackup < ActiveRecord::Base
end

#knock off the header row
dups.shift
#loop through each row
dups.each do |f|
  if f[2].to_i == 1
    
    fire = Fire.find(f[0])
    backup_fire = FireBackup.new
    backup_fire.attributes = fire.attributes
    if backup_fire.save 
      puts "Deleting duplicate fire #{f[0]}"
      fire.destroy      
    end
  end
  
end