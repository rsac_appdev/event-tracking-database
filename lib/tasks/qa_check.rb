require 'csv'
puts "loading data"
fires = CSV.read('IDs_4_deletion_nameDups.csv')
unique_event_ids = [] 
kept_fires = []

#knock off the header row
fires.shift
#loop through each row
fires.each do |f|
  #see if we have a '2' in the 3rd column. If so, push the Event ID onto an array
  if f[2].to_i == 2
    kept_fires << f[1]     
  end
  # If the id of the current fire is in the unique_event_ids array, don't include it
  next if unique_event_ids.include?(f[1])
  unique_event_ids.unshift f[1]
  
end

unique_event_ids.each do |u|
  unless kept_fires.include?(u)
    puts "#{u} is not included in your kept fires."
  end
end
