#RSAC ID generator
import math

def generate_event_id(st, lat, lon, ig_date):
  ymd = ig_date.replace('-', '')
  lat_fixed = str("%06.3f" %float(lat)).replace('.', '')
  lon_fixed = str("%07.3f" % math.fabs(float(lon))).replace('.', '')

  event_id = "{0}{1}{2}{3}".format(st, lat_fixed, lon_fixed, ymd )
  return event_id



# unit test
def test_generate_event_id():
  if generate_event_id("AZ", 35.11923, -113.86205, "2013-06-29") == 'AZ3511911386220130629':
    print "passed truncating floats to 3 sig figs"
  else:
    print "failed truncating floats to 3 sig figs"
    print "Expected {0}".format('AZ3511911386220130629')
    actual = generate_event_id("AZ", 35.19923, -113.86205, "2013-06-29")
    print "Got      {0}".format(actual)

  if generate_event_id("AZ", 35.11923, -113.86205, "2013-06-29") == 'AZ3511911386220130629':
    print "passed rounding floats to 3 sig figs"
  else:
    print "failed rounding floats to 3 sig figs"
    print "Expected {0}".format('AZ3520011386320130629')
    actual = generate_event_id("AZ", 35.19993, -113.86295, "2013-06-29")
    print "Got      {0}".format(actual)

  if generate_event_id("AZ", 35, -113, "2013-06-29") == "AZ3500011300020130629":
    print "passed padding floats to 3 sig figs"
  else:
    print "failed padding floats to 3 sig figs"


test_generate_event_id()
