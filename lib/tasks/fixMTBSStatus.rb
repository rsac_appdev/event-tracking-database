require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

CSV.open("2011_fires_table_updates.csv", "r").each do |slice|
  fire = Fire.where("event_id = ?", slice[0].upcase).first
  if fire.nil?
    puts "WARNING! could not find Event ID: #{slice[0].upcase}"
  else
    fire.mtbs_status = slice[1]
    fire.mapping_in_progress = slice[2]
    fire.mtbs_complete = slice[3]
    if ARGV[0] == '-d'
      puts "Fire #{fire.event_id} will be updated."
    else
      if fire.save
        puts "Saved #{fire.event_id}"
      else
        puts "Could not save #{fire.event_id}!"
      end
    end
  end


end
