require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

class Mapping < ActiveRecord::Base
end


def fix_date(date)
  time = Time.parse(date)
  date_string = time.strftime("%Y-%m-%d")
end

CSV.foreach("2011_mappings_2_import_this_is_the_good_1.csv") do |slice|
  fire = Fire.find_by_event_id(slice[50])
  if(fire.nil?)
    puts slice[1]
  else #if we couldn't find the event_id, let's return an error report
    mapping = Mapping.new
    mapping.fire_id = fire.id
    mapping.prefire_sensor = slice[4]
    mapping.prefire_path = slice[5]
    mapping.prefire_row = slice[6]
    mapping.prefire_scene_id = slice[7]
    mapping.postfire_sensor = slice[8]
    mapping.postfire_path = slice[9]
    mapping.postfire_row = slice[10]
    mapping.postfire_scene_id = slice[11]
    mapping.single_scene = slice[12]
    mapping.dnbr_offset = slice[13]
    mapping.threshold1 = slice[14]
    mapping.threshold2 = slice[15]
    mapping.threshold3 = slice[16]
    mapping.program = slice[18]
    mapping.strategy = slice[19]
    mapping.unmappable_reason = slice[20]
    mapping.perimeter_scene_id = slice[21]
    mapping.other_reason = slice[22]
    mapping.perimeter_sensor = slice[23]
    mapping.perimeter_confidence = slice[24]
    mapping.perimeter_comments = slice[25]
    mapping.analysis = slice[26]
    mapping.prefire_date = slice[27]
    mapping.postfire_date = slice[28]
    mapping.perimeter_date = slice[29]
    mapping.pub_date = slice[30]
    mapping.utm_zone = slice[31]
    mapping.final_pixel_size = slice[32]
    mapping.revised = slice[33]
    mapping.status = slice[34]
    mapping.username = slice[35]
    mapping.no_data_thresh = slice[36]
    mapping.greenness_thresh = slice[37]
    mapping.mapping_comments = slice[38]
    mapping.burn_bndy_ac = slice[39]
    mapping.supplementary_sensor = slice[40]
    mapping.supplementary_scene_id = slice[41]
    mapping.supplementary_date = slice[42]
    mapping.veg_type = slice[43]
    mapping.baer_publishable = slice[44]
    mapping.ravg_publishable = slice[45]
    mapping.burn_bndy_lat = slice[46]
    mapping.burn_bndy_lon = slice[47]
    mapping.burn_bndy_alb_x = slice[48]
    mapping.burn_bndy_alb_y = slice[49]
    if mapping.save
      puts "Saved mapping for #{fire.incident_name} - #{fire.event_id}"
    end
  end


end
