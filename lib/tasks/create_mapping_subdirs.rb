require 'JSON'
require 'net/http'
require 'fileutils'

url_string = "http://svinetfc6.fs.fed.us/rsacfod/mappings/all_by_event.json"
url = URI.parse(url_string)

response = Net::HTTP.get_response(url)

JSON.parse(response.body).each do |res|
  year = res["event_id"][13..16]
  path = "/Volumes/mtbs/rdas_change_detection/event_prods/fire/#{year}/#{res["event_id"]}/#{res["program"]}_#{res["id"]}"
  # path = "/tmp/mtbs/rdas_change_detection/event_prods/fire/#{year}/#{res["event_id"]}/#{res["program"]}_#{res["id"]}"
  FileUtils.mkdir_p path
  # puts path
end

