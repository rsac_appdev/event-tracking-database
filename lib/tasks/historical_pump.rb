
require 'rubygems'
require 'net/http'
require 'yaml'
require 'active_record'


# select 
#     *
# from
#     imsr_imsr_209_incidents
# WHERE
#     status = 'F'
# AND 
#     type_inc = 'WF'
# AND
#     area >= 450
# group by incident_number
# order by report_date 
# LIMIT 0, 1000000

# let's get the states straight
# Fire threshold for the Western states is 900 acres
# Fire threshold for the Eastern states (and Hawaii) is 45 acres
STATES = %w[AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY]

WESTERN_STATES = %w[AK WA OR CA MT ID NV AZ UT WY CO NM ND SD NE KS OK TX]
#EASTERN_STATES = STATES - WESTERN_STATES

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

class Agency < ActiveRecord::Base
end

class HistoricalIncident < ActiveRecord::Base  
end

class Pathrow < ActiveRecord::Base
end

# Setup incidents array
incidents = []

# historical_incidents = HistoricalIncident.where("status != 'I' AND type_inc = 'WF' AND area >= 450")
all_incidents = HistoricalIncident.group('INCIDENT_NUMBER')

incident_numbers = []
historical_incidents = []

all_incidents.each do |inci|
  incident_numbers << inci.INCIDENT_NUMBER
end


incident_numbers.each do |num|
  distinct_incident = HistoricalIncident.where("INCIDENT_NUMBER = ?", num).order("LAST_EDIT desc").limit(1).first
  # distinct_incident = HistoricalIncident.find_by_sql("SELECT * FROM historical_incidents WHERE INCIDENT_NUMBER = '#{num}' ORDER BY LAST_EDIT DESC LIMIT 1")
  historical_incidents << distinct_incident
end

# counter = 0
# historical_incidents.each do |i|
#   counter = counter+1
#   longitude = i.LONGITUDE.to_f*-1
#   puts "#{counter}, #{i.INCIDENT_NAME},#{i.INCIDENT_NUMBER},#{i.AREA},#{i.LATITUDE},#{longitude}"
# end

# clean up incidents based on acreage
historical_incidents.each do |i|
  if WESTERN_STATES.include?(i.OWNERSHIP_STATE)
    if i.AREA.to_f > 900
      incidents << i
      # longitude = i.LONGITUDE.to_f*-1
      # puts "#{counter}, #{i.INCIDENT_NAME.upcase},#{i.INCIDENT_NUMBER.upcase},#{i.AREA},#{i.LATITUDE},#{longitude}"
    end
  else
    incidents << i 
    # longitude = i.LONGITUDE.to_f*-1
    # puts "#{counter}, #{i.INCIDENT_NAME.upcase},#{i.INCIDENT_NUMBER.upcase},#{i.AREA},#{i.LATITUDE},#{longitude}"
  end
end

puts "loading historical data into the database..."
incidents.each do |incident|
  f = Fire.new
  f.created_at = incident.LAST_EDIT
  f.updated_at = incident.LAST_EDIT
  f.ig_state = incident.OWNERSHIP_STATE
  f.ig_lat = incident.LATITUDE
  f.ig_long = incident.LONGITUDE.to_f*-1
  # ig_date = incident.START_DATE.split(' ')[0]
  unless incident.START_DATE.nil?
    ig_date = incident.START_DATE.iso8601.to_s.split('T')[0]
    ymd = Time.parse(ig_date).strftime("%Y%m%d")
    #build the RSAC ID
    lat_array = f.ig_lat.to_s.split('.')
    lat_1 = lat_array[0]
    lat_2 = lat_array[1][0..1]
    long_array = incident.LONGITUDE.to_s.split('.') #could also do f.ig_long.abs
    long_1 = long_array[0]
    long_2 = long_array[1][0..1]
    f.rsac_id = "#{f.ig_state}#{lat_1}#{lat_2}#{long_1}#{long_2}#{ymd}"
  end
  f.incident_id = incident.INCIDENT_NUMBER.upcase
  f.incident_name = incident.INCIDENT_NAME.upcase
  f.incident_type = incident.TYPE_INC
  f.active_incident  = 0
  unless incident.START_DATE.nil?
    f.ig_date = incident.START_DATE.iso8601.to_s.split('T')[0]
  end
  f.area_burned = incident.AREA
  f.percent_contained = incident.P_CONTAIN
  unless incident.EXP_CONTAIN.nil?
    f.expected_containment_date = incident.EXP_CONTAIN.iso8601.to_s.split('T')[0]
  end
  f.ig_admin = incident.OWNERSHIP_UNITID
  f.ig_agency = "#{f.ig_state}-#{f.ig_admin}"
  unless incident.REPORT_DATE.nil?
    f.report_date = incident.REPORT_DATE.iso8601.to_s.split('T')[0]
  end
  f.fuels = incident.FUELS
  f.general_location = incident.LOCATION
  #usfs lookup
  st_unit_id = f.incident_id[0..5]
  s = Agency.find_by_st_unit_id(st_unit_id)
  unless s.nil? 
    f.ig_usfs_region = s.unit_name
    f.fregion = s.fregion    
  end
  
  # Let's get the city and state based on Lat/Long:
  # url = URI.parse('http://maps.googleapis.com/maps/api/geocode/json?latlng='+f.ig_lat.to_s+','+f.ig_long.to_s+'&sensor=false')
  # response = Net::HTTP.get_response(url)

  # data = response.body # store the body in an object

  # result = JSON.parse(data)

  # city_state_zip = result['results'][1]['formatted_address']

  # address_parts = city_state_zip.split(',')

  # city = address_parts[0]
  # state = address_parts[1][1..3]
  # f.city = city
  # f.state = state  

  # Let's also get the path/row for Landsat
  pathrow = Pathrow.find_by_sql("SELECT path, row FROM pathrows WHERE WITHIN(POINT(#{f.ig_long}, #{f.ig_lat}), the_geom)")
  # pathrow = Pathrow.find_by_sql("SELECT path, row FROM pathrows WHERE WITHIN(POINT(-120.429722,44.931111), the_geom)")
  f.path1 = pathrow[0].path
  f.row1 = pathrow[0].row
  if pathrow.length > 1
    f.path2 = pathrow[1].path
    f.row2 = pathrow[1].row
  end
  if f.save
    puts "Successfully ingested #{f.incident_name}."
  else
    puts "Error ingesting data!"
  end
end