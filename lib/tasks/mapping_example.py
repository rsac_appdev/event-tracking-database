import urllib2
import json
import sys

mapping_id = sys.argv[1]

url = "http://svinetfc6.fs.fed.us/rsacfod/mappings/" + mapping_id + ".json"
handle = urllib2.urlopen(url)
response = handle.read().decode('utf8')
#returns a dict
data = json.loads(response)

#define the attributes we plan to use:
program = data['program']
pre_scene_id = data['prefire_scene_id']
pre_sensor = data['prefire_sensor']
post_scene_id = data['postfire_scene_id']
post_sensor = data['postfire_sensor']


print "Program: " + program
print "Prefire Scene: " + pre_scene_id
print "Prefire Sensor: " + pre_sensor
print "Postfire Scene: " + post_scene_id
print "Postfire Sensor: " + post_sensor
