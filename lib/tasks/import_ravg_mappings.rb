require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

class Mapping < ActiveRecord::Base
end


def fix_date(date)
  time = Time.parse(date)
  date_string = time.strftime("%Y-%m-%d")
end

CSV.open("2012RAVG_4_ETD_dos.csv").each do |slice|
  fire = Fire.find_by_event_id(slice[1])
  if(fire.nil?)
    # puts "Cannot find fire #{slice[2]} - #{slice[1]}"
  else #if we couldn't find the event_id, let's return an error report
    mapping = Mapping.new
    mapping.fire_id = fire.id
    mapping.prefire_sensor = slice[3]
    mapping.prefire_date = fix_date(slice[4])
    mapping.prefire_path = slice[5] unless slice[5].nil?
    mapping.prefire_row = slice[6] unless slice[6].nil?
    mapping.prefire_scene_id = slice[7]
    mapping.postfire_sensor = slice[8]
    mapping.postfire_date = fix_date(slice[9])
    mapping.postfire_path = slice[10] unless slice[10].nil?
    mapping.postfire_row = slice[11] unless slice[11].nil?
    mapping.postfire_scene_id = slice[12]
    mapping.single_scene = "0"
    mapping.program = slice[15]
    mapping.analysis = slice[18]
    mapping.status = "complete"
    fire.ravg_status = "complete"

    if mapping.save
      fire.save!
      puts "Saved mapping for #{fire.incident_name} - #{fire.event_id}"
    end
  end


end