# Gems required for this stack include:
# activerecord-jdbc-adapter version <= 1.2.2.1
# activerecord-jdbcmysql-adapter version <= 1.2.2.1
# jdbc-mysql version <= 5.1.13

require 'rubygems'
require 'net/http'
require 'yaml'
require 'active_record'
require 'logger'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Import < ActiveRecord::Base
end

class Fire < ActiveRecord::Base
end

class Agency < ActiveRecord::Base
end

class Pathrow < ActiveRecord::Base
end

#initialize the incidents array
incidents = []

#let's get the last part of the part of the filename
#based on the date. No use doing this over and over in a loop!
# ymd = Time.now.strftime("%Y%m%d")

#set up logging
logger = Logger.new('209pump.log', 'weekly')

url = URI.parse('http://fam.nwcg.gov/fam-web/hist_209/get_rsac_209_1')

response = Net::HTTP.start(url.host, url.port) do |http|
  http.get(url.path)
end

content = response.body # store the body in an object

content = content.gsub("\r\n", " ") # remove all windows CR/LFs
content = content.gsub("\n", "^^") # convert EOLs to delimeter

content_array = content.split('^^') # create an array from the monolithic string

content_array.each_slice(23) {|x| incidents << x}

logger.info "############# Start of 209 data #################"
puts "loading data from ICS209 into the database..."
incidents.each do |incident|
  #if incident_id exists in DB, update the record
  # f = Fire.find_by_sql(["SELECT * FROM fires WHERE incident_id = ? ORDER BY created_at DESC LIMIT 1", incident[1].upcase])
  f = Fire.where(:incident_id => incident[1].upcase).last
  # if ID doesn't exist OR if it does and the name isn't the same
  if f.nil? or f.incident_name.upcase != incident[0].upcase
    logger.info "Incident #{incident[0].upcase} ID: #{incident[1]} was not found in the database. Creating a new record."
    f = Fire.new
    save_message = "New Incident Saved"
    f.created_at = Time.now
    f.srcdb = 'ics209'
    unless incident[10].blank?
      f.ig_state = incident[10]
    else
      f.ig_state = incident[1][0..1]
    end
    f.ig_lat = "%.3f" % incident[13].to_s
    f.ig_long = "-" + "%.3f" % incident[14].to_s
    #build the RSAC ID
    ymd = Time.parse(incident[5]).strftime("%Y%m%d")
    lat_as_float = incident[13].to_f
    lat_array = lat_as_float.to_s.split('.')
    lat_1 = lat_array[0].rjust(2,'0')
    lat_2 = lat_array[1][0..2].ljust(3,'0')
    long_as_float = incident[14].to_f
    long_array = long_as_float.to_s.split('.')
    long_1 = long_array[0].rjust(3,'0')
    long_2 = long_array[1][0..2].ljust(3,'0')
    f.event_id = "#{f.ig_state}#{lat_1}#{lat_2}#{long_1}#{long_2}#{ymd}"
  else
    save_message = "Updating existing incident #{incident[0].upcase} with ID: #{incident[1]}"
  end
  f.incident_id = incident[1].upcase
  f.incident_name = incident[0].upcase
  f.incident_type = incident[7]
  f.active_incident  = incident[2]
  f.ig_date = Time.parse(incident[5]).strftime("%Y-%m-%d")
  f.ig_lat = "%.3f" % incident[13].to_s
  f.ig_long = "-" + "%.3f" % incident[14].to_s
  f.area_burned = incident[16]
  f.percent_contained = incident[18]
  f.expected_containment_date = Time.parse(incident[19]).strftime("%Y-%m-%d")
  f.ig_state = incident[10]
  f.ig_admin = incident[11]
  f.ig_agency = "#{f.ig_state}-#{f.ig_admin}"
  f.report_date = Time.parse(incident[3]).strftime("%Y-%m-%d")
  f.fuels = incident[20]
  f.general_location = incident[15]
  f.updated_at = Time.now
  #usfs lookup
  id = incident[1]
  st_unit_id = id[0..5]
  s = Agency.find_by_st_unit_id(st_unit_id)
  if s.nil?
    logger.fatal "#{incident[4]} has an incorrect or non-existent ufs_unit_id"
  else
    f.ig_usfs_region = s.unit_name
    f.fregion = s.fregion
  end
  # Let's get the city and state based on Lat/Long:
  url = URI.parse('http://maps.googleapis.com/maps/api/geocode/json?latlng='+f.ig_lat.to_s+','+f.ig_long.to_s+'&sensor=false')
  response = Net::HTTP.get_response(url)

  data = response.body # store the body in an object

  result = JSON.parse(data)

  city_state_zip = result['results'][1]['formatted_address']

  address_parts = city_state_zip.split(',')

  city = address_parts[0]
  state = address_parts[1][1..3]
  f.city = city
  f.state = state
  if f.ig_state.to_s != state.to_s
    logger.info "The lat/long of this incident is in #{state} but the ICS209 is reporting it as #{f.ig_state}."
  end
     # Let's also get the path/row for Landsat
  pathrow = Pathrow.find_by_sql("SELECT path, row FROM pathrows WHERE WITHIN(GeomFromText('POINT(#{f.ig_long} #{f.ig_lat})'), the_geom)")
  # pathrow = Pathrow.find_by_sql("SELECT path, row FROM pathrows WHERE WITHIN(GeomFromText('Point(-82.46 30.54)'), the_geom)")

  f.path1 = pathrow[0].path
  f.row1 = pathrow[0].row
  if pathrow.length > 1
    f.path2 = pathrow[1].path
    f.row2 = pathrow[1].row
  end
  if f.save
    logger.info save_message
  else
    logger.fatal "could not save incident " + incident[1]
  end
end

logger.info "############# End of 209 data #################"
