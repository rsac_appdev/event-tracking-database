require 'rubygems'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

def check_threshold(state, acres)
  states = %w[AL AK AZ AR CA CO CT DE FL GA ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY]

  western_states = %w[AK WA OR CA HI MT ID NV AZ UT WY CO NM ND SD NE KS OK TX]
  #EASTERN_STATES = STATES - WESTERN_STATES
  if western_states.include?(state)
    #it's a western state
    if acres.to_f >= 900
      return 1
    end
  else
    #its NOT a western state
    if acres.to_f >= 450
      return 1
    end
  end
  return 0;
end



# fires = Fire.all
fires = Fire.where("id >= 13459")

fires.each do |fire|
  fire.above_mapping_threshold = check_threshold(fire.ig_state, fire.area_burned)

  if fire.save
    puts "#{fire.incident_name} in #{fire.ig_state} burned #{fire.area_burned} acres threshold: #{fire.above_mapping_threshold}"
  else
    puts "#{fire.incident_name} could not be updated!"
  end
end
