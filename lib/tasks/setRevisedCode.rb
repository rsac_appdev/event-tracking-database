require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Mapping < ActiveRecord::Base
end

CSV.foreach("revised_code.csv") do |slice|
  mapping = Mapping.find_by_id(slice[0])
  if(mapping.nil?)
    puts "mapping id #{slice[0]} could not be found in the mappings table!"
  else #if we couldn't find the event_id, let's return an error report
    mapping.revised_code = slice[1]
    if mapping.save
      puts "Saved mapping for #{mapping.id} "
    end
  end


end

