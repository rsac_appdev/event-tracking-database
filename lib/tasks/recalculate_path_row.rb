require 'yaml'
require 'active_record'
require 'logger'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

class PrimaryPathrow < ActiveRecord::Base
end


def get_path_rows(lat,lng)
  primary_pathrow_combos = PrimaryPathrow.find_by_sql("SELECT path, row FROM primary_pathrows WHERE WITHIN(GeomFromText('POINT(#{lng} #{lat})'), SHAPE)")

  unless primary_pathrow_combos.length == 0
    pathrows = Hash.new
    pathrows[:path1] = primary_pathrow_combos[0].path
    pathrows[:row1] = primary_pathrow_combos[0].row
  end

  return pathrows
end

fires = Fire.all

fires.each do |fire|
  pr = get_path_rows(fire.ig_lat,fire.ig_long)
  fire.path1 = pr[:path1]
  fire.row1 = pr[:row1]

  if fire.save
    puts "#{fire.event_id} updated"
  else
    puts "#{fire.event_id} could not be updated!"
  end
end