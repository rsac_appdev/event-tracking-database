require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'
require 'net/http'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

class Agency < ActiveRecord::Base
end

class Pathrow < ActiveRecord::Base
end

################ Utility methods ###############################################

def fix_date(date)
  time = Date.strptime date, '%m/%d/%y'
  date_string = time.strftime("%Y-%m-%d")
end

def generate_event_id(state,lat,lng,ig_date)
  #build the RSAC ID
  time = Date.strptime ig_date, '%m/%d/%y'
  ymd = time.strftime("%Y%m%d")
  lng = lng.gsub('-', '')
  fixed_lat = ("%2.3f" % lat).to_s.gsub('.', '')
  fixed_lng = ("%07.3f" % lng).to_s.gsub('.', '')
  event_id = "#{state}#{fixed_lat}#{fixed_lng}#{ymd}"
  return event_id
end

def get_path_rows(lat,lng)
end

################################################################################

puts "Running import..."

CSV.foreach("TX-2Jim4ETDingest.csv") do |slice|
  incident = Fire.new
  ig_date = fix_date(slice[5])
  incident.incident_name = slice[3].upcase
  puts "Attempting to parse #{incident.incident_name}"
  incident.ig_lat = "%.3f" % slice[8].to_s
  incident.ig_long = "%.3f" % slice[9].to_s
  if slice[11].nil?
    cs = geocode(incident.ig_lat,incident.ig_long)
    puts "hitting the Google to get the State for incident #{incident.incident_name}."
    incident.city = cs[:city]
    incident.state = cs[:state]
    incident.event_id = generate_event_id(incident.state, slice[8], slice[9], slice[5])
    incident.ig_state = incident.state.strip!
  else
    incident.event_id = generate_event_id(slice[11], slice[8], slice[9], slice[5])
    incident.ig_state = slice[11].strip!
  end
  incident.incident_type = slice[4]
  incident.ig_date = ig_date
  incident.area_burned = slice[10]

  incident.comment = slice[19].to_s
  if slice[16] == 'y'
    incident.part_of_complex = 1
  else
    incident.part_of_complex = 0
  end
  incident.updated_at = Time.now
  pr = get_path_rows(incident.ig_lat,incident.ig_long)
  unless pr.nil?
    incident.path1 = pr[:path1]
    incident.row1 = pr[:row1]
    if pr.has_key?(:path2)
      incident.path2 = pr[:path2]
      incident.row2 = pr[:row2]
    end
  end

  incident.srcdb = slice[12]

  if incident.save
    puts "Saved incident #{incident.event_id}"
  end
end
