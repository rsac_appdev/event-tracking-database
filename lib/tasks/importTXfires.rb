require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

def fix_date(date)
  time = Time.parse(date)
  date_string = time.strftime("%Y-%m-%d")
end

def generate_event_id(state,lat,lng,ig_date)
  #build the RSAC ID
  time = Date.strptime ig_date, '%m/%d/%y'
  ymd = time.strftime("%Y%m%d")
  # ymd = Time.parse(ig_date).strftime("%Y%m%d")
  lng = lng.gsub('-', '')
  fixed_lat = ("%2.3f" % lat).to_s.gsub('.', '')
  fixed_lng = ("%07.3f" % lng).to_s.gsub('.', '')
  event_id = "#{state}#{fixed_lat}#{fixed_lng}#{ymd}"
  return event_id
end

CSV.open("TX-2Jim4ETDingest.csv", "r").each do |slice|
  fire = Fire.new
  fire.ig_state = slice[0]
  fire.ig_lat = "%.3f" % slice[1].to_s
  fire.ig_long = "%.3f" % slice[2].to_s
  time = Date.strptime slice[3], '%m/%d/%y'
  ymd = time.strftime("%Y-%m-%d")
  fire.ig_date = time
  fire.area_burned = slice[4]
  fire.comment = slice[5]
  fire.srcdb = slice[6]
  fire.incident_name = slice[7]
  fire.event_id = generate_event_id(fire.ig_state, fire.ig_lat, fire.ig_long, slice[3])
  if ARGV[0] == '-d'
    puts "Saving fire #{fire.incident_name} - #{fire.event_id} from #{fire.ig_lat} & #{fire.ig_long}"
  else
    if fire.save
      puts "Saved #{fire.event_id}"
    else
      puts "Could not save #{fire.event_id}!"
    end
  end


end
