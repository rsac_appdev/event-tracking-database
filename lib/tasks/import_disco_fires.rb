require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

class Fire < ActiveRecord::Base
end

def fix_date(date)
  time = Time.parse(date)
  date_string = time.strftime("%Y-%m-%d")
end

def generate_event_id(state,lat,lng,ig_date)
  #build the RSAC ID
  ymd = Time.parse(ig_date).strftime("%Y%m%d")
  lng = lng.gsub('-', '')
  fixed_lat = ("%2.3f" % lat).to_s.gsub('.', '')
  fixed_lng = ("%07.3f" % lng).to_s.gsub('.', '')
  event_id = "#{state}#{fixed_lat}#{fixed_lng}#{ymd}"
  return event_id
end

CSV.open("2011_ks_ok_t3.csv", "r").each do |slice|
  fire = Fire.new
  fire.incident_name = "UNNAMED"
  fire.ig_state = slice[1]
  fire.ig_lat = "%.3f" % slice[2].to_s
  fire.ig_long = "%.3f" % slice[3].to_s
  fire.ig_date = slice[0]
  fire.area_burned = slice[4]
  fire.comment = slice[5]
  fire.event_id = generate_event_id(fire.ig_state, fire.ig_lat, fire.ig_long, slice[0])
  fire.srcdb = "DISCO"
  if ARGV[0] == '-d'
    puts "Saving fire #{fire.incident_name} - #{fire.event_id} from #{fire.ig_lat} & #{fire.ig_long}"
  else
    if fire.save
      puts "Saved #{fire.event_id}"
    else
      puts "Could not save #{fire.event_id}!"
    end
  end


end