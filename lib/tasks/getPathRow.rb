require 'net/http'
require 'json'

def get_path_row(lat, lng)
  #takes lat/lng combo and returns a ruby object representing path/row/state
  #param1: (float) lat
  #param2: (float) lng
  #RETURNS:
  #(obj) path: (int)
  #      row: (int)
  #      state: (string)

  service_url = 'http://svinetfc6.fs.fed.us/geocode'
  full_url = service_url + "/" + lat.to_s + "/" + lng.to_s

  uri = URI.parse(full_url)
  response = Net::HTTP.get_response(uri)

  case response
    when Net::HTTPSuccess
      return JSON.parse(response.body)
    when Net::HTTPRedirection
      return nil
    else
      return nil
  end

end
