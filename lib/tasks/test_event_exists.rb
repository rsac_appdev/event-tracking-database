require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

events = CSV.read('2012_event_ids.csv')

class Fire < ActiveRecord::Base
end

events.shift
#loop through each row
events.each do |e|

  fire = Fire.where("event_id = ?", e[0])
  puts "Event ID #{e[0]} not found" if fire.empty?

end