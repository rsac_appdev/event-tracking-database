require 'rubygems'
require 'csv'
require 'yaml'
require 'active_record'

#active record DB connection
dbconfig = YAML::load(File.open('database.yml'))
ActiveRecord::Base.establish_connection(dbconfig)

events = CSV.read('Event_ID_replacement.csv')

class Fire < ActiveRecord::Base
end

events.shift
#loop through each row
events.each do |e| 
  fire = Fire.find_by_event_id(e[0])
  puts "Replacing Event ID #{e[0]} with #{e[1].upcase}"
  fire.event_id = e[1].upcase
  fire.ig_lat = e[2]
  fire.ig_long = e[3]
  fire.ig_date = e[4]
  fire.save
end
